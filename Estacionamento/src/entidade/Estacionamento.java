package entidade;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Estacionamento {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	private int vagas;
	private String vlUnidadeEixo;
	private String vlUnidadePassageiro;
	private String vlCaminhaoHora;
	
	private String vlCaminhaoMensal;
	private String vlPasseioHora;

	private String vlPasseioMensal;
	private String vlMotoHora;

	private String vlMotoMensal;
	private String vlTransporteHora; //somente por hora

	private String vlTransporteMensal;

	
	public Estacionamento(){
		
	}

	
	
	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public int getVagas() {
		return vagas;
	}


	public void setVagas(int vagas) {
		this.vagas = vagas;
	}


	public String getVlUnidadeEixo() {
		return vlUnidadeEixo;
	}


	public void setVlUnidadeEixo(String vlUnidadeEixo) {
		this.vlUnidadeEixo = vlUnidadeEixo;
	}


	public String getVlUnidadePassageiro() {
		return vlUnidadePassageiro;
	}


	public void setVlUnidadePassageiro(String vlUnidadePassageiro) {
		this.vlUnidadePassageiro = vlUnidadePassageiro;
	}


	public String getVlCaminhaoHora() {
		return vlCaminhaoHora;
	}


	public void setVlCaminhaoHora(String vlCaminhaoHora) {
		this.vlCaminhaoHora = vlCaminhaoHora;
	}





	public String getVlCaminhaoMensal() {
		return vlCaminhaoMensal;
	}


	public void setVlCaminhaoMensal(String vlCaminhaoMensal) {
		this.vlCaminhaoMensal = vlCaminhaoMensal;
	}


	public String getVlPasseioHora() {
		return vlPasseioHora;
	}


	public void setVlPasseioHora(String vlPasseioHora) {
		this.vlPasseioHora = vlPasseioHora;
	}


	


	public String getVlPasseioMensal() {
		return vlPasseioMensal;
	}


	public void setVlPasseioMensal(String vlPasseioMensal) {
		this.vlPasseioMensal = vlPasseioMensal;
	}


	public String getVlMotoHora() {
		return vlMotoHora;
	}


	public void setVlMotoHora(String vlMotoHora) {
		this.vlMotoHora = vlMotoHora;
	}





	public String getVlMotoMensal() {
		return vlMotoMensal;
	}


	public void setVlMotoMensal(String vlMotoMensal) {
		this.vlMotoMensal = vlMotoMensal;
	}


	public String getVlTransporteHora() {
		return vlTransporteHora;
	}


	public void setVlTransporteHora(String vlTransporteHora) {
		this.vlTransporteHora = vlTransporteHora;
	}


	


	public String getVlTransporteMensal() {
		return vlTransporteMensal;
	}


	public void setVlTransporteMensal(String vlTransporteMensal) {
		this.vlTransporteMensal = vlTransporteMensal;
	}


	

	
	

}
