package entidade;

import intefaces.TipoVeiculoCalculo;

public class Veiculo {
	private int id;
	private TipoVeiculoCalculo tipoVeiculo;
	private String placa;
	private String tipo;
	private String modelo;
	private String mes;
	private int eixos;
	private int capacidade;
	private String entradaHorario;
	private String anoEntrada;
	private String saidaHorario;
	private String dataRegistroEntrada;
	private String dataRegistroSaida;
	private String entradaHoraMillisegundos;// para efeito de c�lculo apenas
	private String saidaHoraMillisegundos;// para efeito de c�lculo apenas
	private String duracaoHora;
	private String valorPago;
	


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public int getEixos() {
		return eixos;
	}

	public void setEixos(int eixos) {
		this.eixos = eixos;
	}

	public int getCapacidade() {
		return capacidade;
	}

	public void setCapacidade(int capacidade) {
		this.capacidade = capacidade;
	}

	public String getEntradaHorario() {
		return entradaHorario;
	}

	public void setEntradaHorario(String entradaHorario) {
		this.entradaHorario = entradaHorario;
	}

	public String getSaidaHorario() {
		return saidaHorario;
	}

	public void setSaidaHorario(String saidaHorario) {
		this.saidaHorario = saidaHorario;
	}

	public String getDataRegistroEntrada() {
		return dataRegistroEntrada;
	}

	public void setDataRegistroEntrada(String dataRegistro) {
		this.dataRegistroEntrada = dataRegistro;
	}

	public String getEntradaHora() {
		return entradaHoraMillisegundos;
	}

	public void setEntradaHora(String entradaHora) {
		this.entradaHoraMillisegundos = entradaHora;
	}

	public String getSaidaHora() {
		return saidaHoraMillisegundos;
	}

	public void setSaidaHora(String saidaHora) {
		this.saidaHoraMillisegundos = saidaHora;
	}

	public String getDuracaoHora() {
		return duracaoHora;
	}

	public void setDuracaoHora(String duracaoHora) {
		this.duracaoHora = duracaoHora;
	}

	public Veiculo(){
		
	}
	

	public TipoVeiculoCalculo getTipoVeiculo() {
		return tipoVeiculo;
	}

	public void setTipoVeiculo(TipoVeiculoCalculo tipoVeiculo) {
		this.tipoVeiculo = tipoVeiculo;
	}
	public Veiculo(String modelo,String placa, String tipo, String entradaHorario,String data, String entradaHora,String mes){//passeio e moto
		this.modelo = modelo;
		this.placa = placa;
		this.tipo = tipo;
		this.entradaHorario=entradaHorario;
		this.dataRegistroEntrada=data;
		this.entradaHoraMillisegundos=entradaHora;
		this.mes=mes;
	}

	public String getDataRegistroSaida() {
		return dataRegistroSaida;
	}

	public void setDataRegistroSaida(String dataRegistroSaida) {
		this.dataRegistroSaida = dataRegistroSaida;
	}

	public String getValorPago() {
		return valorPago;
	}

	public void setValorPago(String valorPago) {
		this.valorPago = valorPago;
	}

	public String getAnoEntrada() {
		return anoEntrada;
	}

	public void setAnoEntrada(String anoEntrada) {
		this.anoEntrada = anoEntrada;
	}
	

}
