package Relatorios;

import java.util.List;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

public class fabricaDeRelatorios {
	public void exportarRelatorio(List lista, String nome){
		try {
			
			JasperReport report = JasperCompileManager .compileReport("/relatorios2/"+nome+".jrxml");
		//JRBeanCollectionDataSource 
		JasperPrint print = JasperFillManager.fillReport(report, null, new JRBeanCollectionDataSource(lista));
		  

		JasperExportManager.exportReportToPdfFile(print, "/relatorios2/"+nome+".pdf");
		
		} catch (JRException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	public void visualizarRelatorio(List lista, String nome){
try {
			
			JasperReport report = JasperCompileManager .compileReport("/relatorios2/"+nome+".jrxml");
		//JRBeanCollectionDataSource 
		JasperPrint print = JasperFillManager.fillReport(report, null, new JRBeanCollectionDataSource(lista));
		JasperViewer jv = new JasperViewer(print, false);  
		          jv.setTitle("Relatório");    
		          jv.setVisible(true);  


		
		} catch (JRException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
}
