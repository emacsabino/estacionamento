package Relatorios;

public class ComprovanteDeEntrada {
	String placa;
	String horarioDeEntrada;
	String entrada;
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getHorarioDeEntrada() {
		return horarioDeEntrada;
	}
	public void setHorarioDeEntrada(String horarioDeEntrada) {
		this.horarioDeEntrada = horarioDeEntrada;
	}
	public String getEntrada() {
		return entrada;
	}
	public void setEntrada(String entrada) {
		this.entrada = entrada;
	}
	

}
