package Relatorios;

public class MovimentacaoTotal {
	private String placa;
	private String hentrada;
	private String hsaida;
	private String dtentrada;
	private String dtsaida;
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getHentrada() {
		return hentrada;
	}
	public void setHentrada(String hentrada) {
		this.hentrada = hentrada;
	}
	public String getHsaida() {
		return hsaida;
	}
	public void setHsaida(String hsaida) {
		this.hsaida = hsaida;
	}
	public String getDtentrada() {
		return dtentrada;
	}
	public void setDtentrada(String dtentrada) {
		this.dtentrada = dtentrada;
	}
	public String getDtsaida() {
		return dtsaida;
	}
	public void setDtsaida(String dtsaida) {
		this.dtsaida = dtsaida;
	}
	

}
