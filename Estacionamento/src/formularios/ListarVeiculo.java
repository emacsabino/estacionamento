package formularios;

import intefaces.TempoImplements;
import intefaces.TipoVeiculoCalculo;
import intefaces.dao.VeiculoJDBCDAO;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JSeparator;

import java.awt.Font;
import java.awt.Color;
import java.awt.SystemColor;

import javax.swing.JButton;

import entidade.Veiculo;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.border.LineBorder;

import Relatorios.ComprovanteDeSaida;
import Relatorios.fabricaDeRelatorios;

public class ListarVeiculo {
	JFrame tela = new JFrame("Dados do Ve�culo");
	Veiculo movel = new Veiculo();
	JButton btnConfirmarPagamento = new JButton("Confirmar Pagamento");
	private JTextField _placa;
	private JTextField _modelo;
	private JTextField _tipo;
	private JTextField _horaEntrada;
	private JTextField _horaSaida;
	private JTextField _capacidade;
	private JTextField _eixos;
	private JTextField _valorAPagar;
	private JTextField idPlaca;
	private JTextField duracao;
	private String tempoInicial,tempoFinal;
	private String valorPagar;
	private JTextField DataEntrada;
	private JTextField DataSaida;
	private int eix,cap;
	/**
	 * @wbp.parser.entryPoint
	 */
	public void exibir(){
		tela.setSize(716, 436);
		tela.getContentPane().setLayout(null);
		
		JLabel lblPlaca = new JLabel("PLACA");
		lblPlaca.setBounds(34, 81, 40, 14);
		tela.getContentPane().add(lblPlaca);
		
		JLabel lblModelo = new JLabel("MODELO");
		lblModelo.setBounds(242, 78, 72, 14);
		tela.getContentPane().add(lblModelo);
		
		_placa = new JTextField();
		_placa.setBounds(79, 78, 122, 20);
		tela.getContentPane().add(_placa);
		_placa.setColumns(10);
		
		_modelo = new JTextField();
		_modelo.setBounds(324, 75, 107, 20);
		tela.getContentPane().add(_modelo);
		_modelo.setColumns(10);
		
		JLabel lblTipo = new JLabel("TIPO");
		lblTipo.setBounds(455, 78, 40, 14);
		tela.getContentPane().add(lblTipo);
		
		_tipo = new JTextField();
		_tipo.setBounds(505, 75, 122, 20);
		tela.getContentPane().add(_tipo);
		_tipo.setColumns(10);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 106, 680, 2);
		tela.getContentPane().add(separator);
		
		JLabel lblHorrioDeEntrada = new JLabel("Hor\u00E1rio de Entrada");
		lblHorrioDeEntrada.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblHorrioDeEntrada.setBounds(154, 181, 134, 27);
		tela.getContentPane().add(lblHorrioDeEntrada);
		
		_horaEntrada = new JTextField();
		_horaEntrada.setBounds(299, 185, 107, 20);
		tela.getContentPane().add(_horaEntrada);
		_horaEntrada.setColumns(10);
		
		JLabel lblHorrioDeSada = new JLabel("Hor\u00E1rio de Sa\u00EDda");
		lblHorrioDeSada.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblHorrioDeSada.setBounds(154, 222, 125, 14);
		tela.getContentPane().add(lblHorrioDeSada);
		
		_horaSaida = new JTextField();
		_horaSaida.setBounds(299, 216, 107, 20);
		tela.getContentPane().add(_horaSaida);
		_horaSaida.setColumns(10);
		
		JLabel lblCapacidadeDeTransporte = new JLabel("Capacidade de Transporte");
		lblCapacidadeDeTransporte.setBounds(154, 126, 134, 14);
		tela.getContentPane().add(lblCapacidadeDeTransporte);
		
		_capacidade = new JTextField();
		_capacidade.setBounds(299, 123, 46, 20);
		tela.getContentPane().add(_capacidade);
		_capacidade.setColumns(10);
		
		JLabel lblQuantidadeDeEixos = new JLabel("Quant. Eixos do Caminh\u00E3o");
		lblQuantidadeDeEixos.setBounds(154, 156, 134, 14);
		tela.getContentPane().add(lblQuantidadeDeEixos);
		
		_eixos = new JTextField();
		_eixos.setBounds(299, 154, 46, 20);
		tela.getContentPane().add(_eixos);
		_eixos.setColumns(10);
		
		JLabel lblValorAPagar = new JLabel("Valor a Pagar");
		lblValorAPagar.setForeground(Color.RED);
		lblValorAPagar.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblValorAPagar.setBounds(154, 271, 107, 27);
		tela.getContentPane().add(lblValorAPagar);
		
		_valorAPagar = new JTextField();
		_valorAPagar.setBounds(299, 276, 107, 20);
		tela.getContentPane().add(_valorAPagar);
		_valorAPagar.setColumns(10);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 54, 680, 2);
		tela.getContentPane().add(separator_1);
		
		JLabel lblDigiteAPlaca = new JLabel("Digite a placa ou ID do ve\u00EDculo");
		lblDigiteAPlaca.setForeground(SystemColor.desktop);
		lblDigiteAPlaca.setFont(new Font("Palatino Linotype", Font.BOLD, 15));
		lblDigiteAPlaca.setBounds(79, 11, 237, 32);
		tela.getContentPane().add(lblDigiteAPlaca);
		
		idPlaca = new JTextField();
		idPlaca.setBounds(299, 17, 150, 20);
		tela.getContentPane().add(idPlaca);
		idPlaca.setColumns(10);
		idPlaca.addFocusListener(new FocusAdapter() {  
            public void focusGained(FocusEvent evt) {  //limpar campos quando a caixa de texto recebe foco
            	limparCampos();
            	
            }  
       });
		
		JButton btnLocalizar = new JButton("LOCALIZAR");
		btnLocalizar.addActionListener(new ActionListener() {
			VeiculoJDBCDAO buscar = new VeiculoJDBCDAO();
			
			
			int identificador;
			public void actionPerformed(ActionEvent e) {
				
				if ( buscar.convert(idPlaca.getText())){
					identificador = Integer.parseInt(idPlaca.getText());
					movel = buscar.findByID(identificador);
				}else{
					movel =buscar.findByPlaca(idPlaca.getText());
				}
				
				if (movel!=null){
				_placa.setText(movel.getPlaca());
				_modelo.setText(movel.getModelo());
				DataEntrada.setText(movel.getDataRegistroEntrada());
				
				TipoVeiculoCalculo statu = TipoVeiculoCalculo.valueOf(movel.getTipo());//pega o tipo de ve�culo para localizar no enum
				movel.setTipoVeiculo(statu);//atribui o tipo de veiculo a classe VEICULO
				
				_tipo.setText(movel.getTipo());
				_capacidade.setText(String.valueOf(movel.getCapacidade()));
				_eixos.setText(String.valueOf(movel.getEixos()));
				_horaEntrada.setText(movel.getEntradaHorario());
				tempoInicial = movel.getEntradaHora();
				eix = movel.getEixos();
				cap = movel.getCapacidade();
				
				
				
				}else{
					JOptionPane.showMessageDialog(null,"Esse ve�culo n�o est� no estacionamento");
				}
				
								
			}
		});
		btnLocalizar.setBounds(497, 16, 111, 23);
		tela.getContentPane().add(btnLocalizar);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblNewLabel.setBounds(62, 335, 565, 43);
		tela.getContentPane().add(lblNewLabel);
		
		JButton btnCalcularTempo = new JButton("Calcular Tempo");
		btnCalcularTempo.addActionListener(new ActionListener() {
			TempoImplements tempo = new TempoImplements();
			double durHora,durMinuto;
			
			
			public void actionPerformed(ActionEvent arg0) {
				tempoFinal = tempo.saidaHoraMillisegundos();
				durHora = tempo.duracaoHora(tempoFinal, tempoInicial);
				
				durMinuto=tempo.duracaoMinuto(tempoFinal, tempoInicial);
				
				DataSaida.setText(tempo.dataRegistro());
				duracao.setText(tempo.duracao(tempoFinal, tempoInicial));
				
/*strategy*/	valorPagar=movel.getTipoVeiculo().veiculo((int)durHora,(int) durMinuto,eix,cap);//implementa o strategy calculando o valor dinamicamente
				
				_valorAPagar.setText(valorPagar);
				_horaSaida.setText(tempo.saidaHorario());
				movel.setDuracaoHora(duracao.getText());
				movel.setDataRegistroSaida(DataSaida.getText());
				movel.setValorPago(_valorAPagar.getText());
				movel.setSaidaHorario(_horaSaida.getText());
			
				btnConfirmarPagamento.setEnabled(true);
			}
		});
		btnCalcularTempo.setBounds(101, 347, 140, 23);
		tela.getContentPane().add(btnCalcularTempo);
		btnConfirmarPagamento.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				VeiculoJDBCDAO inserirHistorico = new VeiculoJDBCDAO();
				inserirHistorico.saveVeiculoHistorico(movel);
				inserirHistorico.deleteVeiculoID(movel.getId());
				comprovante(_placa.getText(), _horaEntrada.getText(),_horaSaida.getText(), DataEntrada.getText(),DataSaida.getText(),_valorAPagar.getText());
				limparCampos();
				btnConfirmarPagamento.setEnabled(false);
				
			}
		});
		
		
		btnConfirmarPagamento.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnConfirmarPagamento.setForeground(Color.BLUE);
		btnConfirmarPagamento.setBounds(430, 347, 162, 23);
		btnConfirmarPagamento.setEnabled(false);
		tela.getContentPane().add(btnConfirmarPagamento);
		
		JLabel lblDurao = new JLabel("Dura\u00E7\u00E3o");
		lblDurao.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblDurao.setBounds(154, 249, 89, 14);
		tela.getContentPane().add(lblDurao);
		
		duracao = new JTextField();
		duracao.setColumns(10);
		duracao.setBounds(299, 247, 175, 20);
		tela.getContentPane().add(duracao);
		
		JLabel lblDataEntrada = new JLabel("Data Entrada");
		lblDataEntrada.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblDataEntrada.setBounds(430, 181, 97, 27);
		tela.getContentPane().add(lblDataEntrada);
		
		DataEntrada = new JTextField();
		DataEntrada.setColumns(10);
		DataEntrada.setBounds(541, 185, 107, 20);
		tela.getContentPane().add(DataEntrada);
		
		JLabel lblDataSaida = new JLabel("Data Saida");
		lblDataSaida.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblDataSaida.setBounds(430, 216, 81, 27);
		tela.getContentPane().add(lblDataSaida);
		
		DataSaida = new JTextField();
		DataSaida.setColumns(10);
		DataSaida.setBounds(541, 220, 107, 20);
		tela.getContentPane().add(DataSaida);
		tela.setVisible(true);
	}
   private void limparCampos(){
	   _placa.setText(null);
		_modelo.setText(null);
		_tipo.setText(null);
		_capacidade.setText(null);
		_eixos.setText(null);
		_horaEntrada.setText(null);
		_valorAPagar.setText(null);
		duracao.setText(null);
		_horaEntrada.setText(null);
		_horaSaida.setText(null);
		DataEntrada.setText(null);
		DataSaida.setText(null);
   }
   private void comprovante(String placa, String horarioEntrada,String horarioSaida, String dataEntrada,String dataSaida, String valor){
		fabricaDeRelatorios relat = new fabricaDeRelatorios();
		List<ComprovanteDeSaida> lista = new ArrayList<>();
		ComprovanteDeSaida veiculo = new ComprovanteDeSaida();
		veiculo.setPlaca(placa);
		veiculo.setHorarioEntrada(horarioEntrada);
		veiculo.setEntrada(dataEntrada);
		veiculo.setHorarioSaida(horarioSaida);
		veiculo.setSaida(dataSaida);
		veiculo.setValor(valor);
		lista.add(veiculo);
		relat.visualizarRelatorio(lista, "comprovanteSaida");
	}
}
