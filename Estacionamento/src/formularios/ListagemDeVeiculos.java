package formularios;

import intefaces.dao.VeiculoJDBCDAO;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.Container;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

import entidade.Veiculo;

public class ListagemDeVeiculos {
	JFrame tela = new JFrame("Listagem de Veiculos");
	private JTable table;
	/**
	 * @wbp.parser.entryPoint
	 */
	public void listagem(){
		final DefaultTableModel modelo = new DefaultTableModel();
		
		tela.setLocationRelativeTo(null);
		modelo.addColumn("ID");
		modelo.addColumn("Placa");
		modelo.addColumn("Tipo");
		modelo.addColumn("Capacidade");
		modelo.addColumn("Eixos");		
		modelo.addColumn("Data de Entrada");
		modelo.addColumn("Horario de Entrada");
		
		
		
		
	
		
		List <Veiculo> ListaCarros = new ArrayList<>();
		VeiculoJDBCDAO exibirVeiculos = new VeiculoJDBCDAO();
		ListaCarros = exibirVeiculos.find();
		
		
		for(Veiculo p:ListaCarros){
			
			int id = p.getId();
			String placa = p.getPlaca();
			String tipo = p.getTipo();
			int capacidade = p.getCapacidade();
			int eixos = p.getEixos();
			String data = p.getDataRegistroEntrada();
			String horario = p.getEntradaHorario();
			modelo.addRow(new Object[]{id,placa,tipo,capacidade,eixos,data,horario});
			
		}
		tela.getContentPane().setLayout(null);
		
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(25, 5, 707, 380);
		tela.getContentPane().add(scrollPane);
		
		table = new JTable(modelo);
		table.setForeground(Color.YELLOW);
		table.setBackground(Color.GRAY);
		table.setFont(new Font("Tahoma", Font.BOLD, 12));
		JTableHeader cabecalho = table.getTableHeader();
		cabecalho.setFont(new Font("Tahoma", Font.BOLD, 9));
		scrollPane.setViewportView(table);
		table.setPreferredScrollableViewportSize(new Dimension(500,50));
		table.setFillsViewportHeight(true);
		tela.setSize(767, 435);
		tela.setVisible(true);
	
		
	}
	
}
