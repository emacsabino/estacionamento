package formularios;

import intefaces.dao.EstacionamentoJPADAO;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JSeparator;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;

import java.awt.Label;

import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import java.awt.Button;

import javax.swing.JButton;

import entidade.Estacionamento;
import entidade.Veiculo;





import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Properties;

public class TabelaPreco {
	JFrame formulario = new JFrame("Atualizar Pre�os");
	private JTextField transporteMensal;
	private JTextField caminhaoMensal;
	private JTextField passeioMensal;
	private JTextField motoMensal;
	private JTextField transporteHora;
	private JTextField caminhaoHora;
	private JTextField passeioHora;
	private JTextField motoHora;
	private JTextField vlUnidadeTransporte;
	private JTextField vlUnidadeEixo;
	private JTextField vagas;
	/**
	 * @wbp.parser.entryPoint
	 */
	public void exibirFormulario(){
		formulario.getContentPane().setBackground(new Color(250, 250, 210));
		formulario.setSize(842, 418);
		formulario.getContentPane().setLayout(null);
		
		JLabel lblAtualizaoDePreos = new JLabel("ATUALIZA\u00C7\u00C3O DE PRE\u00C7OS E VAGAS");
		lblAtualizaoDePreos.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblAtualizaoDePreos.setForeground(Color.BLUE);
		lblAtualizaoDePreos.setHorizontalAlignment(SwingConstants.CENTER);
		lblAtualizaoDePreos.setHorizontalTextPosition(SwingConstants.CENTER);
		lblAtualizaoDePreos.setBounds(10, 11, 355, 14);
		formulario.getContentPane().add(lblAtualizaoDePreos);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 36, 583, 2);
		formulario.getContentPane().add(separator);
		
		Label label = new Label("VALORES MENSAL");
		label.setFont(new Font("Dialog", Font.BOLD, 10));
		label.setBackground(Color.GRAY);
		label.setForeground(Color.YELLOW);
		label.setBounds(10, 48, 583, 22);
		formulario.getContentPane().add(label);
		
		JLabel lblTransporte = new JLabel("Transporte");
		lblTransporte.setBounds(10, 87, 66, 14);
		formulario.getContentPane().add(lblTransporte);
		
		transporteMensal = new JTextField();
		transporteMensal.setBounds(81, 84, 73, 20);
		formulario.getContentPane().add(transporteMensal);
		transporteMensal.setColumns(10);
		
		JLabel lblCaminho = new JLabel("Caminh\u00E3o");
		lblCaminho.setBounds(164, 87, 66, 14);
		formulario.getContentPane().add(lblCaminho);
		
		caminhaoMensal = new JTextField();
		caminhaoMensal.setBounds(240, 84, 73, 20);
		formulario.getContentPane().add(caminhaoMensal);
		caminhaoMensal.setColumns(10);
		
		JLabel lblPasseio = new JLabel("Passeio");
		lblPasseio.setBounds(337, 87, 46, 14);
		formulario.getContentPane().add(lblPasseio);
		
		passeioMensal = new JTextField();
		passeioMensal.setBounds(386, 84, 73, 20);
		formulario.getContentPane().add(passeioMensal);
		passeioMensal.setColumns(10);
		
		JLabel lblMoto = new JLabel("Moto");
		lblMoto.setBounds(483, 87, 34, 14);
		formulario.getContentPane().add(lblMoto);
		
		motoMensal = new JTextField();
		motoMensal.setBounds(520, 84, 73, 20);
		formulario.getContentPane().add(motoMensal);
		motoMensal.setColumns(10);
		
		Label label_6 = new Label("VALORES HORA");
		label_6.setForeground(Color.YELLOW);
		label_6.setFont(new Font("Dialog", Font.BOLD, 10));
		label_6.setBackground(Color.GRAY);
		label_6.setBounds(10, 131, 583, 22);
		formulario.getContentPane().add(label_6);
		
		JLabel label_7 = new JLabel("Transporte");
		label_7.setBounds(10, 177, 66, 14);
		formulario.getContentPane().add(label_7);
		
		transporteHora = new JTextField();
		transporteHora.setColumns(10);
		transporteHora.setBounds(81, 177, 73, 20);
		formulario.getContentPane().add(transporteHora);
		
		JLabel label_8 = new JLabel("Caminh\u00E3o");
		label_8.setBounds(164, 180, 66, 14);
		formulario.getContentPane().add(label_8);
		
		caminhaoHora = new JTextField();
		caminhaoHora.setColumns(10);
		caminhaoHora.setBounds(240, 177, 73, 20);
		formulario.getContentPane().add(caminhaoHora);
		
		JLabel label_9 = new JLabel("Passeio");
		label_9.setBounds(337, 180, 46, 14);
		formulario.getContentPane().add(label_9);
		
		passeioHora = new JTextField();
		passeioHora.setColumns(10);
		passeioHora.setBounds(386, 177, 73, 20);
		formulario.getContentPane().add(passeioHora);
		
		JLabel label_10 = new JLabel("Moto");
		label_10.setBounds(483, 180, 34, 14);
		formulario.getContentPane().add(label_10);
		
		motoHora = new JTextField();
		motoHora.setColumns(10);
		motoHora.setBounds(520, 180, 73, 20);
		formulario.getContentPane().add(motoHora);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 311, 583, 2);
		formulario.getContentPane().add(separator_1);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBorder(new LineBorder(new Color(0, 0, 255), 2));
		lblNewLabel.setBounds(640, 264, 161, 71);
		formulario.getContentPane().add(lblNewLabel);
		
		JButton btnLimparValores = new JButton("OK");
		btnLimparValores.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				limparCampos();
			}
		});
		btnLimparValores.setBounds(697, 70, 53, 23);
		formulario.getContentPane().add(btnLimparValores);
		
		JLabel lblLimparValores = new JLabel("Zerar Valores");
		lblLimparValores.setHorizontalAlignment(SwingConstants.CENTER);
		lblLimparValores.setBounds(640, 45, 161, 14);
		formulario.getContentPane().add(lblLimparValores);
		
		JLabel label_11 = new JLabel("");
		label_11.setBorder(new LineBorder(new Color(0, 0, 255), 2));
		label_11.setBounds(640, 36, 163, 71);
		formulario.getContentPane().add(label_11);
		
		JLabel lblArmazenar = new JLabel("Armazenar");
		lblArmazenar.setHorizontalAlignment(SwingConstants.CENTER);
		lblArmazenar.setBounds(642, 274, 161, 14);
		formulario.getContentPane().add(lblArmazenar);
		
		JButton button = new JButton("OK");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				atualizarDados();
				limparCampos();
			}
		});
		button.setBounds(697, 299, 53, 23);
		formulario.getContentPane().add(button);
		
		Label label_12 = new Label("ADICIONAIS");
		label_12.setForeground(Color.YELLOW);
		label_12.setFont(new Font("Dialog", Font.BOLD, 10));
		label_12.setBackground(Color.GRAY);
		label_12.setBounds(10, 221, 583, 22);
		formulario.getContentPane().add(label_12);
		
		JLabel lblUnidadeDeCapacidade = new JLabel("Unidade de Capacidade - Transporte");
		lblUnidadeDeCapacidade.setBounds(10, 264, 220, 14);
		formulario.getContentPane().add(lblUnidadeDeCapacidade);
		
		vlUnidadeTransporte = new JTextField();
		vlUnidadeTransporte.setColumns(10);
		vlUnidadeTransporte.setBounds(240, 261, 73, 20);
		formulario.getContentPane().add(vlUnidadeTransporte);
		
		JLabel lblUnidadeDeEixo = new JLabel("Unidade de Eixo - Caminh\u00E3o");
		lblUnidadeDeEixo.setBounds(323, 264, 161, 14);
		formulario.getContentPane().add(lblUnidadeDeEixo);
		
		vlUnidadeEixo = new JTextField();
		vlUnidadeEixo.setColumns(10);
		vlUnidadeEixo.setBounds(498, 261, 73, 20);
		formulario.getContentPane().add(vlUnidadeEixo);
		
		JLabel lblVagas = new JLabel("VAGAS");
		lblVagas.setForeground(new Color(204, 51, 0));
		lblVagas.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblVagas.setBounds(386, 12, 53, 14);
		formulario.getContentPane().add(lblVagas);
		
		vagas = new JTextField();
		vagas.setColumns(10);
		vagas.setBounds(433, 9, 73, 20);
		formulario.getContentPane().add(vagas);
		
		JButton btnNewButton = new JButton("OK");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				carregarDados();
			}
		});
		btnNewButton.setBounds(697, 173, 53, 23);
		formulario.getContentPane().add(btnNewButton);
		
		JLabel label_13 = new JLabel("");
		label_13.setBorder(new LineBorder(new Color(0, 0, 255), 2));
		label_13.setBounds(638, 142, 163, 71);
		formulario.getContentPane().add(label_13);
		
		JLabel lblCarregarValores = new JLabel("Carregar Valores Atuais");
		lblCarregarValores.setHorizontalAlignment(SwingConstants.CENTER);
		lblCarregarValores.setBounds(640, 150, 161, 14);
		formulario.getContentPane().add(lblCarregarValores);
		carregarDados();
		formulario.setVisible(true);
	}
	private void limparCampos(){
		transporteMensal.setText(null);
		caminhaoMensal.setText(null);
		passeioMensal.setText(null);
		
		
		
		transporteHora.setText(null);
		caminhaoHora.setText(null);
		passeioHora.setText(null);
		motoMensal.setText(null);
		
		motoHora.setText(null);
		vlUnidadeEixo.setText(null);
		vlUnidadeTransporte.setText(null);
		vagas.setText(null);
	}
	private void atualizarDados(){
	long id;
		EstacionamentoJPADAO atualizar = new EstacionamentoJPADAO();
			
			try{
				
				Estacionamento estacionamento;
						
				id=1;
				
			
				estacionamento = atualizar.find();
				
					
					
					
					estacionamento.setVagas(Integer.parseInt(vagas.getText()));
					
					estacionamento.setVlCaminhaoHora(caminhaoHora.getText());
					estacionamento.setVlCaminhaoMensal(caminhaoMensal.getText());
					
					estacionamento.setVlMotoHora(motoHora.getText());
					estacionamento.setVlMotoMensal(motoMensal.getText());
					
					estacionamento.setVlPasseioHora(passeioHora.getText());
					estacionamento.setVlPasseioMensal(passeioMensal.getText());
				
					estacionamento.setVlTransporteHora(transporteHora.getText());
					estacionamento.setVlTransporteMensal(transporteMensal.getText());
					estacionamento.setVlUnidadeEixo(vlUnidadeEixo.getText());
					estacionamento.setVlUnidadePassageiro(vlUnidadeTransporte.getText());
					
					atualizar.update(estacionamento);
					
				
			
				
				
				
			}catch(Exception e){
				JOptionPane.showMessageDialog(null, "Ocorreu um erro na atualiza��o de valores");
				System.out.println(e.getMessage());
			}
		
	}
	private void carregarDados(){
		
		EstacionamentoJPADAO localizar = new EstacionamentoJPADAO();
		Estacionamento estaci = new Estacionamento();
		try{
		
		estaci = localizar.find();
		vagas.setText(String.valueOf(estaci.getVagas()));
		
		caminhaoHora.setText(estaci.getVlCaminhaoHora());
		caminhaoMensal.setText(estaci.getVlCaminhaoMensal());
		
		motoHora.setText(estaci.getVlMotoHora());
		motoMensal.setText(estaci.getVlMotoMensal());
		
		passeioHora.setText(estaci.getVlPasseioHora());
		passeioMensal.setText(estaci.getVlPasseioMensal());
		
		transporteHora.setText(estaci.getVlTransporteHora());
		transporteMensal.setText(estaci.getVlTransporteMensal());
		vlUnidadeEixo.setText(estaci.getVlUnidadeEixo());
		vlUnidadeTransporte.setText(estaci.getVlUnidadePassageiro());
		}catch(Exception e){
			System.out.println(e);
		}
		
	}
}
