package formularios;

import intefaces.dao.RelatorioJDBCDAO;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.border.TitledBorder;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JFormattedTextField;
import javax.swing.UIManager;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JSeparator;
import javax.swing.JRadioButton;
import javax.swing.JTextField;






















import entidade.Veiculo;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;
import Relatorios.PorMesEspecifico;
import Relatorios.fabricaDeRelatorios;
import Relatorios.PorDiaEspecifico;
import Utilitarios.LimitadorCaracteres;

public class RelatorioFinanceiro {
	JFrame tela = new JFrame("Relatórios");
	JRadioButton dataEspecifica;
	JRadioButton mesEspecifico;
	JLabel lblDigiteUmaData;
	String [] opcaoMES = {"janeiro","fevereiro","marco","abril","maio","junho","julho","agosto","setembro","outubro","novembro","dezembro"};
	String [] opcaoANO = {"2014","2015"};
	private JFormattedTextField campo;
	LimitadorCaracteres padronizar;
	private JComboBox mes;
	private JComboBox ano;
	private JLabel escolhaMESeANO;
	private JButton btnDiaEspecifico;
	JButton btnMesEspecifico;
	
	private List<PorDiaEspecifico> lista = new ArrayList<>();
	private List<PorMesEspecifico> lista2 = new ArrayList<>();
	/**
	 * @wbp.parser.entryPoint
	 */
	public void formulario (){
		tela.getContentPane().setBackground(Color.WHITE);
		tela.setSize(334, 308);
		tela.getContentPane().setLayout(null);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 78, 293, 2);
		tela.getContentPane().add(separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 38, 293, 2);
		tela.getContentPane().add(separator_1);
		
		JLabel lblOpes = new JLabel("OP\u00C7\u00D5ES");
		lblOpes.setBounds(127, 11, 67, 14);
		tela.getContentPane().add(lblOpes);
		
		 dataEspecifica = new JRadioButton("DIA ESPEC\u00CDFICO");
		dataEspecifica.setBounds(20, 48, 131, 23);
		tela.getContentPane().add(dataEspecifica);
		
		mesEspecifico = new JRadioButton("M\u00CAS ESPEC\u00CDFICO");
		mesEspecifico.setBounds(176, 47, 127, 23);
		tela.getContentPane().add(mesEspecifico);
		
		 lblDigiteUmaData = new JLabel("Digite uma data no formato XX/XX/XXXX");
		lblDigiteUmaData.setBounds(50, 102, 235, 14);
		lblDigiteUmaData.setVisible(false);
		tela.getContentPane().add(lblDigiteUmaData);
		
		campo = new JFormattedTextField();
		campo.setBounds(110, 127, 74, 20);
		
		tela.getContentPane().add(campo);
		campo.setColumns(10);
		campo.setVisible(false);
		padronizar = new LimitadorCaracteres();
		padronizar.dataformato(campo);
		ButtonGroup bg = new ButtonGroup(); 
		
		bg.add(dataEspecifica);
		bg.add(mesEspecifico);
		
		mes = new JComboBox(opcaoMES);
		mes.setBounds(60, 127, 81, 20);
		tela.getContentPane().add(mes);
		
		ano = new JComboBox(opcaoANO);
		ano.setBounds(179, 127, 67, 20);
		tela.getContentPane().add(ano);
		
		escolhaMESeANO = new JLabel("Escolha o m\u00EAs e ano");
		escolhaMESeANO.setBounds(93, 102, 131, 14);
		tela.getContentPane().add(escolhaMESeANO);
		
		btnDiaEspecifico = new JButton("OK");
		
		btnDiaEspecifico.setBounds(105, 191, 89, 23);
		tela.getContentPane().add(btnDiaEspecifico);
		
		btnMesEspecifico = new JButton("OK");
		btnMesEspecifico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				RelatorioJDBCDAO buscar =new RelatorioJDBCDAO();
				
				lista2=buscar.findPorMes((String)mes.getSelectedItem(), (String)ano.getSelectedItem());
				fabricaDeRelatorios gerar = new fabricaDeRelatorios();
				
				gerar.visualizarRelatorio(lista2, "mesEspecifico");
			}
		});
		btnMesEspecifico.setBounds(105, 191, 89, 23);
		tela.getContentPane().add(btnMesEspecifico);
		
		escolhaMESeANO.setVisible(false);
		mes.setVisible(false);
		ano.setVisible(false);
		btnMesEspecifico.setVisible(false);
		btnDiaEspecifico.setVisible(false);
		dataEspecifica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (dataEspecifica.isSelected()){
					lblDigiteUmaData.setVisible(true);
					campo.setVisible(true);
					mes.setVisible(false);
					ano.setVisible(false);
					escolhaMESeANO.setVisible(false);
					btnDiaEspecifico.setVisible(true);
					btnMesEspecifico.setVisible(false);
					
				}
				}

			});
		mesEspecifico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (mesEspecifico.isSelected()){
					lblDigiteUmaData.setVisible(false);
					campo.setVisible(false);
					mes.setVisible(true);
					ano.setVisible(true);
					escolhaMESeANO.setVisible(true);
					btnDiaEspecifico.setVisible(false);
					btnMesEspecifico.setVisible(true);
					
				}
				}

			});
		
		btnDiaEspecifico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RelatorioJDBCDAO buscar =new RelatorioJDBCDAO();
			
				lista=buscar.findPorData(campo.getText());
				fabricaDeRelatorios gerar = new fabricaDeRelatorios();
				
				gerar.visualizarRelatorio(lista, "diaEspecifico");
			
				
				
			
			}
		});
	
		
		tela.setVisible(true);
	}
}
