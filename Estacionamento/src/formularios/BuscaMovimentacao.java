package formularios;

import intefaces.dao.RelatorioJDBCDAO;

import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;

import java.awt.Color;

import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;

import Relatorios.MovimentacaoTotal;
import Relatorios.fabricaDeRelatorios;
import Utilitarios.LimitadorCaracteres;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

public class BuscaMovimentacao {
	JFrame formulario = new JFrame("Busca de Movimentação");
	List<MovimentacaoTotal> lista = new ArrayList<>();
	private JFormattedTextField campo;
	/**
	 * @wbp.parser.entryPoint
	 */
	public void exibirForm(){
		formulario.setSize(300, 288);
		formulario.getContentPane().setLayout(null);
		
		JLabel label = new JLabel("");
		label.setBorder(new LineBorder(Color.LIGHT_GRAY, 2));
		label.setBounds(24, 25, 232, 204);
		formulario.getContentPane().add(label);
		
		JLabel lblDigiteAPlaca = new JLabel("DIGITE A PLACA");
		lblDigiteAPlaca.setHorizontalAlignment(SwingConstants.CENTER);
		lblDigiteAPlaca.setBounds(24, 45, 232, 14);
		formulario.getContentPane().add(lblDigiteAPlaca);
		
		campo = new JFormattedTextField();
		campo.setBounds(82, 84, 126, 20);
		formulario.getContentPane().add(campo);
		campo.setColumns(10);
		LimitadorCaracteres limitar = new LimitadorCaracteres();
		limitar.limitador(campo);
		
		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				RelatorioJDBCDAO buscar =new RelatorioJDBCDAO();
				
				lista=buscar.findPorPlaca(campo.getText());
				fabricaDeRelatorios gerar = new fabricaDeRelatorios();
				
				gerar.visualizarRelatorio(lista, "movimentacaototal");
			}
		});
		btnOk.setBounds(100, 149, 89, 23);
		formulario.getContentPane().add(btnOk);
		formulario.setVisible(true);
	}
}
