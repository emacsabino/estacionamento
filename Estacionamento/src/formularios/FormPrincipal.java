package formularios;

import intefaces.dao.BancoDeDados;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import java.awt.Font;

import javax.swing.JSeparator;



public class FormPrincipal {
	public ImageIcon imagem;
	JFrame tela = new JFrame("Estacionamento");
	/**
	 * @wbp.parser.entryPoint
	 */
	public void exibirform(){
		BancoDeDados db = new BancoDeDados();
		if (db.criarBanco("veiculos")) {
	           db.criarTabelaMovimento();
	           db.criarTabelaHistorico();
	           db.criarTabelaJPA();
	           
	           
	       }
		
		
        
		tela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel painel = new JPanel();//Componente para agrupar na janela outros componentes
		painel.setBackground(Color.WHITE);
        JMenuBar barra = new JMenuBar();//cria o objeto para inserir a barra de menus
        barra.setBorderPainted(false);
        barra.setBackground(Color.WHITE);
    	JLabel label = new JLabel(imagem = new ImageIcon(getClass().getResource("/IMAGEM/image.png")));
    	label.setBackground(Color.WHITE);
		label.setBounds(0, 0, 805, 395);
		
		
		JMenu localizar = new JMenu("LOCALIZAR");
		localizar.setBackground(Color.WHITE);
		JMenu novo = new JMenu("ENTRADAS");
		novo.setBackground(Color.WHITE);
		JMenu relatorios = new JMenu("RELATORIOS");
		relatorios.setBackground(Color.WHITE);
	
		tela.setContentPane(painel);//insere no objeto tela
		painel.setLayout(null);
		painel.add(label);
		
		JLabel lblNewLabel = new JLabel("Teresina - PI");
		lblNewLabel.setBounds(0, 407, 805, 20);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel.setForeground(new Color(255, 0, 0));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		painel.add(lblNewLabel);
		
		JLabel lblTodosOsDireitos = new JLabel("Todos os Direitos Reservados");
		lblTodosOsDireitos.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTodosOsDireitos.setForeground(Color.RED);
		lblTodosOsDireitos.setHorizontalAlignment(SwingConstants.CENTER);
		lblTodosOsDireitos.setBounds(10, 428, 795, 20);
		painel.add(lblTodosOsDireitos);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(0, 393, 805, 2);
		painel.add(separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 11, 785, 2);
		painel.add(separator_1);
		
		JLabel lblEmacsabinohotmailcom = new JLabel("emacsabino@hotmail.com");
		lblEmacsabinohotmailcom.setBounds(642, 24, 153, 20);
		painel.add(lblEmacsabinohotmailcom);
		
		JLabel lblContato = new JLabel("CONTATO");
		lblContato.setBounds(681, 11, 114, 14);
		painel.add(lblContato);
		tela.setSize(821,533);//dimensiona o tamanho da tela
		
		tela.setJMenuBar(barra);//insere a barra na tela principal
		
		 JMenuItem registrarVeiculo = new JMenuItem("REGISTRAR VE�CULO");
		registrarVeiculo.setForeground(Color.blue);
		
		JMenuItem relatorioFinanceiro = new JMenuItem("FINANCEIRO");
		JMenuItem relatorioMovimentacao = new JMenuItem("MOVIMENTA��O");
		relatorioMovimentacao.setForeground(Color.BLUE);
		relatorioFinanceiro.setForeground(Color.blue);
		JMenuItem localizarEspecifico =new JMenuItem("ESPECIFICO");
		localizarEspecifico.setForeground(Color.blue);
		JMenuItem localizarTodos = new JMenuItem("TODOS");
		localizarTodos.setForeground(Color.blue);
		
		
		barra.add(novo);//insere os objetos criados na barra
		barra.add(localizar);
		barra.add(relatorios);
		localizar.add(localizarTodos);//insere op��es dentro das primeiras op��es
		localizar.add(localizarEspecifico);
		
		novo.add(registrarVeiculo);
		relatorios.add(relatorioFinanceiro);
		relatorios.add(relatorioMovimentacao);
		
		JMenu mnOutros = new JMenu("OUTROS");
		barra.add(mnOutros);
		
		JMenuItem mntmAtualizarValores = new JMenuItem("ATUALIZAR VALORES");
		mntmAtualizarValores.setForeground(Color.BLUE);
		mnOutros.add(mntmAtualizarValores);
		
		JMenuItem mntmSair = new JMenuItem("SAIR");
		mntmSair.setForeground(Color.BLUE);
		mnOutros.add(mntmSair);
		relatorioMovimentacao.addActionListener(new ActionListener(){
			
			 public void actionPerformed(ActionEvent e){
			
				 BuscaMovimentacao exibir = new BuscaMovimentacao();
				 exibir.exibirForm();
						
					
					
			 }
		 });
		mntmAtualizarValores.addActionListener(new ActionListener(){
			
			 public void actionPerformed(ActionEvent e){
			
				 TabelaPreco exibir = new TabelaPreco();
				 exibir.exibirFormulario();
						
					
					
			 }
		 });
		mntmSair.addActionListener(new ActionListener(){
			
			 public void actionPerformed(ActionEvent e){
			
				System.exit(0);
						
					
					
			 }
		 });
		
		registrarVeiculo.addActionListener(new ActionListener(){
			
			 public void actionPerformed(ActionEvent e){
			
				 Cadastrar exibir = new Cadastrar();
				exibir.entrada();
						
					
					
			 }
		 });
		 localizarEspecifico.addActionListener(new ActionListener(){
				
			 public void actionPerformed(ActionEvent e){
				
				 ListarVeiculo exibir = new ListarVeiculo();
				 exibir.exibir();
				
			 }
		 });
		
		 relatorioFinanceiro.addActionListener(new ActionListener(){
			 
			 public void actionPerformed(ActionEvent e){
				 
				 RelatorioFinanceiro exibir = new RelatorioFinanceiro();
				 exibir.formulario();
					
			 }
		 });
		 localizarTodos.addActionListener(new ActionListener(){
				
			 public void actionPerformed(ActionEvent e){
				 ListagemDeVeiculos exibir = new ListagemDeVeiculos();
				 exibir.listagem();
				
					
			 }
		 });
		
		
	
		
		
		tela.setVisible(true);
	}
}
