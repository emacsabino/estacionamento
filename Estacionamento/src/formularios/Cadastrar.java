package formularios;

import intefaces.TempoImplements;
import intefaces.TipoVeiculoCalculo;
import intefaces.dao.BancoDeDados;
import intefaces.dao.EstacionamentoJPADAO;
import intefaces.dao.VeiculoJDBCDAO;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.Font;

import javax.swing.JSeparator;
import javax.swing.JComboBox;

import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
















import Lista.Tipos;
import Relatorios.ComprovanteDeEntrada;
import Relatorios.fabricaDeRelatorios;
import Utilitarios.LimitadorCaracteres;

import java.awt.Color;




import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextField;
import javax.swing.JButton;














import entidade.Estacionamento;
import entidade.Veiculo;


public class Cadastrar {
	
	JFrame formCadastro = new JFrame("Entradas");
	JLabel lblEstacionamentoLotado = new JLabel("ESTACIONAMENTO LOTADO");
	JLabel lblAviso = new JLabel("AVISO!!");
	private int valorEsta,valorEntrad;
	private static JFormattedTextField placa;
	private static JTextField capacidade;
	private static JTextField data;
	private static JTextField horarioEntrad;
	private static JComboBox comboTipo;
	private static JComboBox comboEixo;
	private static String opcao;
	private static JLabel lblCapacidade;
	private static JLabel lblEixos;
	private static JTextField modelo;
	public static VeiculoJDBCDAO bd = new VeiculoJDBCDAO();
	public List <Veiculo> veiculos = new ArrayList<>();
	private JTextField estaVAGAS;
	private JTextField estaENTRADAS;
	
	/**
	 * @wbp.parser.entryPoint
	 */
	public void entrada(){
		
		
		formCadastro.setSize(736,439);
		formCadastro.getContentPane().setLayout(null);
		LimitadorCaracteres limitarCampo = new LimitadorCaracteres();
		JLabel lblEntradaDeDados = new JLabel("Entrada de Dados");
		lblEntradaDeDados.setFont(new Font("Tennessee Heavy SF", Font.PLAIN, 18));
		lblEntradaDeDados.setHorizontalAlignment(SwingConstants.CENTER);
		lblEntradaDeDados.setBounds(10, 30, 490, 27);
		formCadastro.getContentPane().add(lblEntradaDeDados);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 55, 844, 2);
		formCadastro.getContentPane().add(separator);
		Tipos list = new Tipos();
		 comboTipo = new JComboBox(list.tiposVeiculos);
		comboTipo.setBounds(233, 82, 117, 20);
		formCadastro.getContentPane().add(comboTipo);
		
		JLabel lblTipo = new JLabel("TIPO");
		lblTipo.setForeground(Color.BLUE);
		lblTipo.setFont(new Font("Meiryo", Font.BOLD, 14));
		lblTipo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTipo.setBounds(142, 82, 43, 20);
		formCadastro.getContentPane().add(lblTipo);
		
		JLabel lblPlaca = new JLabel("PLACA");
		lblPlaca.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPlaca.setForeground(Color.BLUE);
		lblPlaca.setBounds(142, 146, 46, 14);
		formCadastro.getContentPane().add(lblPlaca);
		
		placa = new JFormattedTextField();
		
		placa.setBounds(233, 143, 117, 20);
		limitarCampo.limitador(placa);
		formCadastro.getContentPane().add(placa);
		placa.setColumns(10);
		placa.addFocusListener(new FocusAdapter() {  
            public void focusGained(FocusEvent evt) { 
            	
            	verificarLotacao();
            }  
       });
		
		lblEixos = new JLabel("EIXOS");
		lblEixos.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblEixos.setForeground(Color.BLUE);
		lblEixos.setBounds(142, 208, 43, 14);
		formCadastro.getContentPane().add(lblEixos);
		
		comboEixo = new JComboBox(list.valores);
		comboEixo.setBounds(233, 205, 117, 20);
		formCadastro.getContentPane().add(comboEixo);
		
		lblCapacidade = new JLabel("CAPACIDADE");
		lblCapacidade.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCapacidade.setForeground(Color.BLUE);
		lblCapacidade.setBounds(142, 239, 81, 14);
		formCadastro.getContentPane().add(lblCapacidade);
		
		capacidade = new JTextField();
		capacidade.setBounds(233, 236, 117, 20);
		formCadastro.getContentPane().add(capacidade);
		capacidade.setColumns(10);
		
		JLabel lblData = new JLabel("DATA");
		lblData.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblData.setForeground(Color.BLUE);
		lblData.setBounds(142, 270, 46, 14);
		formCadastro.getContentPane().add(lblData);
		
		data = new JTextField();
		data.setBounds(233, 267, 117, 20);
		formCadastro.getContentPane().add(data);
		data.setColumns(10);
		TempoImplements tmp = new TempoImplements();
		data.setText(tmp.dataRegistro());
		
		JLabel lblHorEntrada = new JLabel("HOR. ENTRADA");
		lblHorEntrada.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblHorEntrada.setForeground(Color.BLUE);
		lblHorEntrada.setBounds(142, 301, 92, 14);
		formCadastro.getContentPane().add(lblHorEntrada);
		
		horarioEntrad = new JTextField();
		horarioEntrad.setBounds(233, 298, 117, 20);
		formCadastro.getContentPane().add(horarioEntrad);
		horarioEntrad.setColumns(10);
		horarioEntrad.setText(tmp.entradaHorario());
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 496, 844, 2);
		formCadastro.getContentPane().add(separator_1);
		
		JButton btnSalvar = new JButton("Salvar");
		btnSalvar.setBounds(464, 292, 89, 23);
		formCadastro.getContentPane().add(btnSalvar);
		
		JLabel lblModelo = new JLabel("MODELO");
		lblModelo.setForeground(Color.BLUE);
		lblModelo.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblModelo.setBounds(142, 177, 46, 14);
		formCadastro.getContentPane().add(lblModelo);
		
		modelo = new JTextField();
		modelo.setBounds(233, 174, 117, 20);
		formCadastro.getContentPane().add(modelo);
		modelo.setColumns(10);
		
		
		lblAviso.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblAviso.setForeground(Color.RED);
		lblAviso.setBounds(527, 103, 92, 27);
		formCadastro.getContentPane().add(lblAviso);
		lblAviso.setVisible(false);
		
		
		lblEstacionamentoLotado.setForeground(Color.RED);
		lblEstacionamentoLotado.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblEstacionamentoLotado.setBounds(448, 132, 230, 28);
		formCadastro.getContentPane().add(lblEstacionamentoLotado);
		
		JLabel lblVagas = new JLabel("VAGAS");
		lblVagas.setBounds(517, 11, 46, 14);
		formCadastro.getContentPane().add(lblVagas);
		
		estaVAGAS = new JTextField();
		estaVAGAS.setBounds(573, 8, 63, 20);
		formCadastro.getContentPane().add(estaVAGAS);
		estaVAGAS.setColumns(10);
		
		JLabel lblEntradas = new JLabel("ENTRADAS");
		lblEntradas.setBounds(499, 37, 68, 14);
		formCadastro.getContentPane().add(lblEntradas);
		
		estaENTRADAS = new JTextField();
		estaENTRADAS.setBounds(573, 34, 63, 20);
		formCadastro.getContentPane().add(estaENTRADAS);
		estaENTRADAS.setColumns(10);
		lblEstacionamentoLotado.setVisible(false);
		capacidade.setVisible(false);
		lblCapacidade.setVisible(false);
	comboTipo.addActionListener(new ActionListener(){
		
		public void actionPerformed(ActionEvent e){
		
			opcao=(String)comboTipo.getSelectedItem();
	
		switch (opcao){
		case "MOTO":{
			capacidade.setVisible(false);
			lblCapacidade.setVisible(false);
			lblEixos.setVisible(false);
			comboEixo.setVisible(false);
						
			break;
		}
		case "TRANSPORTE":{
			capacidade.setVisible(true);
			lblCapacidade.setVisible(true);
			lblEixos.setVisible(false);
			comboEixo.setVisible(false);
			capacidade.setText("0");
			break;
		}
		case "CAMINHAO":{
			lblEixos.setVisible(true);
			comboEixo.setVisible(true);
			capacidade.setVisible(false);
			lblCapacidade.setVisible(false);
			break;
		}
		case "PASSEIO":{
			capacidade.setVisible(false);
			lblCapacidade.setVisible(false);
			lblEixos.setVisible(false);
			comboEixo.setVisible(false);
			break;
			
		}
		}
		}});
	btnSalvar.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
			
			opcao=(String)comboTipo.getSelectedItem();
	
		switch (opcao){
		case "MOTO":{
			String nmodelo, nplaca, ntipo,horarioEntrada,ndata,mes,horaEntrada,ano;
			TipoVeiculoCalculo status = TipoVeiculoCalculo.MOTO;
			TempoImplements hEntr = new TempoImplements();
			nmodelo = modelo.getText();
			nplaca = placa.getText();
			ntipo = "MOTO";
			horarioEntrada = horarioEntrad.getText();
			ndata = data.getText();
			ano = hEntr.getAno();
			horaEntrada = hEntr.entradaHoraMillisegundos();
			
			mes = hEntr.mesRegistro();
			comprovante(nplaca,horarioEntrada,ndata);
			Veiculo moto = new Veiculo(nmodelo,nplaca, ntipo, horarioEntrada,ndata, horaEntrada,mes);
			moto.setAnoEntrada(ano);
			bd.saveVeiculoMovimento(moto);
			
			veiculos.add(moto);
			placa.setText(null);
			modelo.setText(null);
			atualizarHora();			
						
			break;
		}
		case "TRANSPORTE":{
			String nmodelo, nplaca, ntipo,horarioEntrada,ndata,mes,horaEntrada,ano;
			int ncapacidade;
			TipoVeiculoCalculo status = TipoVeiculoCalculo.TRANSPORTE;
			TempoImplements hEntr = new TempoImplements();
			
			nmodelo = modelo.getText();
			nplaca = placa.getText();
			ntipo = "TRANSPORTE";
			horarioEntrada = horarioEntrad.getText();
			ndata = data.getText();
			ano = hEntr.getAno();
			ncapacidade = Integer.parseInt(capacidade.getText());
			horaEntrada = hEntr.entradaHoraMillisegundos();
			
			mes = hEntr.mesRegistro();
			comprovante(nplaca,horarioEntrada,ndata);
			Veiculo transp = new Veiculo(nmodelo,nplaca, ntipo, horarioEntrada,ndata, horaEntrada,mes);
			transp.setCapacidade(ncapacidade);
			transp.setAnoEntrada(ano);
			bd.saveVeiculoMovimento(transp);
			
			veiculos.add(transp);
			placa.setText(null);
			modelo.setText(null);
			atualizarHora();
			
			break;
		}
		case "CAMINHAO":{
			String nmodelo, nplaca, ntipo,horarioEntrada,ndata,mes,horaEntrada,ano;
			int neixo;
			TipoVeiculoCalculo status = TipoVeiculoCalculo.CAMINHAO;
			TempoImplements hEntr = new TempoImplements();
			nmodelo = modelo.getText();
			nplaca = placa.getText();
			ntipo = "CAMINHAO";
			horarioEntrada = horarioEntrad.getText();
			ndata = data.getText();
			neixo = Integer.parseInt((String)comboEixo.getSelectedItem());
			horaEntrada = hEntr.entradaHoraMillisegundos();
			
			mes = hEntr.mesRegistro();
			ano = hEntr.getAno();
			comprovante(nplaca,horarioEntrada,ndata);
			Veiculo caminhao =new Veiculo(nmodelo,nplaca, ntipo, horarioEntrada,ndata, horaEntrada,mes);
			caminhao.setAnoEntrada(ano);
			caminhao.setEixos(neixo);
			bd.saveVeiculoMovimento(caminhao);
			
			veiculos.add(caminhao);
			placa.setText(null);
			modelo.setText(null);
			atualizarHora();
			break;
		}
		case "PASSEIO":{
			String nmodelo, nplaca, ntipo,horarioEntrada,ndata,mes,horaEntrada,ano;
			TipoVeiculoCalculo status = TipoVeiculoCalculo.PASSEIO;
			TempoImplements hEntr = new TempoImplements();
			nmodelo = modelo.getText();
			nplaca = placa.getText();
			ntipo = "PASSEIO";
			horarioEntrada = horarioEntrad.getText();
			ndata = data.getText();
			mes = hEntr.mesRegistro();
			ano = hEntr.getAno();
			horaEntrada = hEntr.entradaHoraMillisegundos();
			comprovante(nplaca,horarioEntrada,ndata);
			Veiculo  passeio = new Veiculo(nmodelo,nplaca, ntipo, horarioEntrada,ndata, horaEntrada,mes);
			passeio.setAnoEntrada(ano);
			bd.saveVeiculoMovimento(passeio);
			
			veiculos.add(passeio);
			placa.setText(null);
			modelo.setText(null);
			atualizarHora();
			
			break;
			
		}
		}
		verificarLotacao();
		}});
	formCadastro.setVisible(true); //dar visibilidade ao formulário principal
		
	}
	private void atualizarHora(){
		TempoImplements tmp2 = new TempoImplements();
		horarioEntrad.setText(tmp2.entradaHorario());
	}
	private void verificarLotacao(){
		
		EstacionamentoJPADAO localizar = new EstacionamentoJPADAO();
		Estacionamento estaci = new Estacionamento();
		VeiculoJDBCDAO vagas = new VeiculoJDBCDAO();
		try{
		valorEntrad=vagas.quantEntradas();
		estaENTRADAS.setText(String.valueOf(valorEntrad));
		estaci = localizar.find();
		valorEsta = estaci.getVagas();
		estaVAGAS.setText(String.valueOf(valorEsta));
		if(valorEntrad>=valorEsta){
			lblAviso.setVisible(true);
			lblEstacionamentoLotado.setVisible(true);
		}
		else{
			lblAviso.setVisible(false);
			lblEstacionamentoLotado.setVisible(false);
		}
		}catch(Exception e){
			System.out.println(e);
		}
		
		
	}
	private void comprovante(String placa, String horario, String data){
		fabricaDeRelatorios relat = new fabricaDeRelatorios();
		List<ComprovanteDeEntrada> lista = new ArrayList<>();
		ComprovanteDeEntrada veiculo = new ComprovanteDeEntrada();
		veiculo.setPlaca(placa);
		veiculo.setHorarioDeEntrada(horario);
		veiculo.setEntrada(data);
		lista.add(veiculo);
		relat.visualizarRelatorio(lista, "comprovanteEntrada");
	}
}

	
