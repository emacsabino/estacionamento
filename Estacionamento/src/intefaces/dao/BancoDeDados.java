package intefaces.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.swing.JOptionPane;

import entidade.Estacionamento;





public class BancoDeDados {
	private static String banco;
	public boolean criarBanco(String DBname) {
        boolean resultado = true;
        banco = DBname;
        try {
            Connection conexao = new FabricaDeConexoes().getConnection("");

            String sql = "CREATE DATABASE " + DBname;

            PreparedStatement ps = conexao.prepareStatement(sql);
            ps.execute();
            ps.close();
            conexao.close();
            JOptionPane.showMessageDialog(null, "Banco de Dados  \ncriado com Sucesso!");
        } catch (SQLException ex) {
            resultado = false;
        }
        
        return resultado;
    }

    public void criarTabelaMovimento() {
    	
        try {
        	Connection conexao = new FabricaDeConexoes().getConnection(banco);

            String criaTable = "CREATE TABLE IF NOT EXISTS movimento ("
                    + "ID_CLIENTE INT UNSIGNED AUTO_INCREMENT NOT NULL,"
                    + "MODELO CHAR(10),"
                    + "PLACA VARCHAR(50) UNIQUE NOT NULL,"
                    + "TIPO CHAR(10),"
                    + "CAPACIDADE INT(3),"
                    + "EIXOS INT(2),"
                    + "DataDeRegistroEntrada CHAR(10),"
                    + "HorarioEntrada CHAR(5),"
                    + "EntradaHora CHAR(30),"
                    + "MES CHAR(10),"
                    + "ANO CHAR(10),"
                    + "PRIMARY KEY(ID_CLIENTE))"
                    + "ENGINE MyISAM";

            PreparedStatement ps = conexao.prepareStatement(criaTable);
            ps.execute();
            ps.close();
            conexao.close();
            JOptionPane.showMessageDialog(null, "Tabela Movimento\ncriado com Sucesso!");
            
           

        } catch (SQLException ex) {
        	Logger.getLogger(VeiculoJDBCDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    public void criarTabelaHistorico() {
    	
        try {
        	Connection conexao = new FabricaDeConexoes().getConnection(banco);

            String criaTable = "CREATE TABLE IF NOT EXISTS historico ("
                    + "ID_CLIENTE INT UNSIGNED AUTO_INCREMENT NOT NULL,"
                    + "MODELO CHAR(10),"
                    + "PLACA VARCHAR(50),"
                    + "TIPO CHAR(10),"
                    + "CAPACIDADE INT(3),"
                    + "EIXOS INT(2),"
                    + "DataDeRegistroEntrada CHAR(10),"
                    + "DataDeRegistroSaida CHAR(10),"
                    + "HorarioEntrada CHAR(5),"
                    + "SaidaHorario CHAR(5),"
                    + "EntradaHora CHAR(30),"
                    + "DuracaoHora CHAR(30),"
                    + "MES CHAR(10),"
                    + "ANO CHAR(10),"
                    + "VALOR_PAGO CHAR(20),"
                    + "PRIMARY KEY(ID_CLIENTE))"
                    + "ENGINE MyISAM";

            PreparedStatement ps = conexao.prepareStatement(criaTable);
            ps.execute();
            ps.close();
            conexao.close();
            JOptionPane.showMessageDialog(null, "Tabela historico \ncriado com Sucesso!");
            

        } catch (SQLException ex) {
        	Logger.getLogger(VeiculoJDBCDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
   
	public void criarTabelaJPA(){
    	
		Estacionamento estacionamento = new Estacionamento();
		estacionamento.setVagas(0);
		
		estacionamento.setVlCaminhaoHora("0");
		estacionamento.setVlCaminhaoMensal("0");
		
		estacionamento.setVlMotoHora("0");
		estacionamento.setVlMotoMensal("0");
		
		estacionamento.setVlPasseioHora("0");
		estacionamento.setVlPasseioMensal("0");
		
		estacionamento.setVlTransporteHora("0");
		estacionamento.setVlTransporteMensal("0");
		estacionamento.setVlUnidadeEixo("0");
		estacionamento.setVlUnidadePassageiro("0");
		try{
			EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-veiculo");
			EntityManager em = emf.createEntityManager();
			EntityTransaction tran = em.getTransaction();
			tran.begin();
			em.persist(estacionamento);
			tran.commit();
			em.close();
			emf.close();
			
		}catch(Exception e){
			System.out.println(e);
		}
		
		
    	
    }
}
