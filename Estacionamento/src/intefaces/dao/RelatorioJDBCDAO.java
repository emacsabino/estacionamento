package intefaces.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import Relatorios.MovimentacaoTotal;
import Relatorios.PorDiaEspecifico;
import Relatorios.PorMesEspecifico;

public class RelatorioJDBCDAO implements RelatorioDAO {

	@Override
	public List<PorDiaEspecifico> findPorData(String dia) {
		
		Connection conexao = new FabricaDeConexoes().getConnection("veiculos");
		List<PorDiaEspecifico> listaValores = new ArrayList<>();
		
		try{
			String sql = "SELECT * FROM historico WHERE DataDeRegistroSaida LIKE ? ";
			PreparedStatement stm = conexao.prepareStatement(sql);
			stm.setString(1,dia);
			ResultSet resultado = stm.executeQuery();
			
			
			while (resultado.next()){
				PorDiaEspecifico carros = new PorDiaEspecifico();
				carros.setPLACA(resultado.getString("PLACA"));
				String valor = resultado.getString("VALOR_PAGO");
				carros.setVALOR(Double.parseDouble(valor.replace(",", ".")));
				carros.setDATA(resultado.getString("DataDeRegistroSaida"));
				listaValores.add(carros);
				
				
			}
			
			stm.close();
			conexao.close();
			
		}catch (SQLException ex) {
			Logger.getLogger(VeiculoJDBCDAO.class.getName()).log(
					Level.SEVERE, null, ex);
			throw new RuntimeException();
		}
		
		return listaValores;
	}

	@Override
	public List<MovimentacaoTotal> findPorPlaca(String placa) {
		Connection conexao = new FabricaDeConexoes().getConnection("veiculos");
		List<MovimentacaoTotal> listaValores = new ArrayList<>();
		
		try{
			String sql = "SELECT * FROM historico WHERE PLACA LIKE ? ";
			PreparedStatement stm = conexao.prepareStatement(sql);
			stm.setString(1,placa);
			ResultSet resultado = stm.executeQuery();
			
			
			while (resultado.next()){
				MovimentacaoTotal carros = new MovimentacaoTotal();
				
				carros.setPlaca(resultado.getString("PLACA"));
				carros.setDtentrada(resultado.getString("DataDeRegistroEntrada"));
				carros.setDtsaida(resultado.getString("DataDeRegistroSaida"));
				carros.setHentrada(resultado.getString("HorarioEntrada"));
				carros.setHsaida(resultado.getString("SaidaHorario"));
				
				
				listaValores.add(carros);
				
				
			}
			
			stm.close();
			conexao.close();
			
		}catch (SQLException ex) {
			Logger.getLogger(VeiculoJDBCDAO.class.getName()).log(
					Level.SEVERE, null, ex);
			throw new RuntimeException();
		}
		
		return listaValores;
	}

	@Override
	public List<PorMesEspecifico> findPorMes(String mes, String ano) {
		Connection conexao = new FabricaDeConexoes().getConnection("veiculos");
		List<PorMesEspecifico> listaValores = new ArrayList<>();
		
		try{
			String sql = "SELECT * FROM historico WHERE (MES LIKE ?) AND (ANO LIKE?) ";
			PreparedStatement stm = conexao.prepareStatement(sql);
			stm.setString(1,mes);
			stm.setString(2,ano);
			ResultSet resultado = stm.executeQuery();
			
			
			while (resultado.next()){
				PorMesEspecifico carros = new PorMesEspecifico();
				carros.setPlaca(resultado.getString("PLACA"));
				carros.setMes(resultado.getString("MES"));
				carros.setAno(resultado.getString("ANO"));
				String valor = resultado.getString("VALOR_PAGO");
				carros.setValor(Double.parseDouble(valor.replace(",", ".")));
				
				listaValores.add(carros);
				
				
			}
		
			
			stm.close();
			conexao.close();
			
		}catch (SQLException ex) {
			Logger.getLogger(VeiculoJDBCDAO.class.getName()).log(
					Level.SEVERE, null, ex);
			throw new RuntimeException();
		}
		
		return listaValores;
	}

}
