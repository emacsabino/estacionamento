package intefaces.dao;

import java.util.List;

import Relatorios.MovimentacaoTotal;
import Relatorios.PorDiaEspecifico;
import Relatorios.PorMesEspecifico;


public interface RelatorioDAO {
	public List<PorDiaEspecifico> findPorData(String dia);
	public List<PorMesEspecifico> findPorMes(String mes, String ano);
	public List<MovimentacaoTotal> findPorPlaca(String placa);
	
}
