package intefaces.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.swing.JOptionPane;

import Utilitarios.JPAutil;
import entidade.Estacionamento;

public class EstacionamentoJPADAO implements EstacionamentoDAO {

	@Override
	public Estacionamento find() {
		long id=1;
		Estacionamento estacion = new Estacionamento();
		try{
		JPAutil manager = new JPAutil();
	EntityManager mac = manager.getCurrentEntityManager();
	
	 estacion = mac.find(Estacionamento.class, id);
	 manager.closeCurrentEntityManager();
		}catch(Exception e){
			System.out.println(e);
		}
	
		return estacion;
	}

	@Override
	public void update(Estacionamento estacionamento) {
		try{
		JPAutil manager = new JPAutil();
		EntityManager mac = manager.getCurrentEntityManager();
		EntityTransaction tran = mac.getTransaction();
		tran.begin();
		mac.merge(estacionamento);
		tran.commit();
		manager.closeCurrentEntityManager();
		 JOptionPane.showMessageDialog(null, "Valores Atualizados Com Sucesso!");
		}catch(Exception e){
			System.out.println(e);
		}
		
	}

}

	
