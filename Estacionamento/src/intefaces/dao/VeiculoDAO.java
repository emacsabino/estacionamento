package intefaces.dao;

import java.util.List;

import entidade.Veiculo;


public interface VeiculoDAO {
	public void saveVeiculoMovimento (Veiculo entidade);  
	public void saveVeiculoHistorico (Veiculo entidade);  
	public List<Veiculo> find();
	public Veiculo findByPlaca(String placa);
	public Veiculo findByID(int id);
	public void deleteVeiculoID(int id);
	public int quantEntradas();
	public boolean convert(String valor);
	

}
