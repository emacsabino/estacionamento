package intefaces.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;





import entidade.Veiculo;

public class VeiculoJDBCDAO implements VeiculoDAO {

	@Override
	public void saveVeiculoMovimento(Veiculo entidade) {
		Connection conexao = new FabricaDeConexoes().getConnection("veiculos");

		

			try {

				String sql = "INSERT INTO movimento(MODELO,PLACA,TIPO,CAPACIDADE,EIXOS,DataDeRegistroEntrada,HorarioEntrada,EntradaHora,MES,ANO) values (?,?,?,?,?,?,?,?,?,?)";
				PreparedStatement pstm = conexao.prepareStatement(sql);
				pstm.setString(1, entidade.getModelo());
				pstm.setString(2, entidade.getPlaca());
				pstm.setString(3, entidade.getTipo());
				pstm.setInt(4, entidade.getCapacidade());
				pstm.setInt(5, entidade.getEixos());
				pstm.setString(6, entidade.getDataRegistroEntrada());
				pstm.setString(7, entidade.getEntradaHorario());
				pstm.setString(8, entidade.getEntradaHora());
				pstm.setString(9, entidade.getMes());
				pstm.setString(10, entidade.getAnoEntrada());
				

				pstm.execute();
				pstm.close();

				conexao.close();

			} catch (SQLException ex) {
				Logger.getLogger(VeiculoJDBCDAO.class.getName()).log(
						Level.SEVERE, null, ex);
				throw new RuntimeException();
			}
		
		

		
		
	}

	@Override
	public List<Veiculo> find() {
		Connection conexao = new FabricaDeConexoes().getConnection("veiculos");
		String sql ="SELECT * FROM movimento";
		List<Veiculo> movel = new ArrayList<>();
		try{
			
			PreparedStatement pstm = conexao.prepareStatement(sql);
			ResultSet resultado = pstm.executeQuery();
			while(resultado.next()){
				Veiculo automovel = new Veiculo();
				automovel.setId(resultado.getInt("ID_CLIENTE"));
				automovel.setPlaca(resultado.getString("PLACA"));
				automovel.setTipo(resultado.getString("TIPO"));
				automovel.setModelo(resultado.getString("MODELO"));
				automovel.setCapacidade(resultado.getInt("CAPACIDADE"));
				automovel.setEixos(resultado.getInt("EIXOS"));
				automovel.setDataRegistroEntrada(resultado.getString("DataDeRegistroEntrada"));
				automovel.setEntradaHorario(resultado.getString("HorarioEntrada"));
				
				movel.add(automovel);
				
			}
			
			pstm.close();
			conexao.close();
			
		}catch (SQLException ex) {
			
			Logger.getLogger(VeiculoJDBCDAO.class.getName()).log(
					Level.SEVERE, null, ex);
			throw new RuntimeException();
		}
		
	
		return movel;
	}

	@Override
	public Veiculo findByPlaca(String placa) {
		
		
		Connection conexao = new FabricaDeConexoes().getConnection("veiculos");
		
		Veiculo automovel = new Veiculo();
		
		try{
			String sql = "SELECT * FROM movimento WHERE PLACA LIKE ? ";
			PreparedStatement stm = conexao.prepareStatement(sql);
			stm.setString(1,placa);
			ResultSet resultado = stm.executeQuery();
			
			while (resultado.next()){
				automovel.setId(resultado.getInt("ID_CLIENTE"));
				automovel.setPlaca(resultado.getString("PLACA"));
				automovel.setModelo(resultado.getString("MODELO"));
				automovel.setTipo(resultado.getString("TIPO"));
				automovel.setCapacidade(resultado.getInt("CAPACIDADE"));
				automovel.setEixos(resultado.getInt("EIXOS"));
				automovel.setDataRegistroEntrada(resultado.getString("DataDeRegistroEntrada"));
				automovel.setEntradaHorario(resultado.getString("HorarioEntrada"));
				automovel.setEntradaHora(resultado.getString("EntradaHora"));
				automovel.setMes(resultado.getString("MES"));
				automovel.setAnoEntrada(resultado.getString("ANO"));
				
			}
			if(automovel.getId()==0){
				automovel=null;
			}
			stm.close();
			conexao.close();
			
		}catch (SQLException ex) {
			Logger.getLogger(VeiculoJDBCDAO.class.getName()).log(
					Level.SEVERE, null, ex);
			throw new RuntimeException();
		}
		return automovel;
	}

	

	@Override
	public Veiculo findByID(int id) {
	
	Veiculo automovel = new Veiculo();
	
	Connection conexao = new FabricaDeConexoes().getConnection("veiculos");
	try{
		String SQL = "SELECT*FROM movimento WHERE ID_CLIENTE = "+id;
		PreparedStatement stm = conexao.prepareStatement(SQL);
		ResultSet resultado = stm.executeQuery();
		 
		
		while (resultado.next()){
			automovel.setId(resultado.getInt("ID_CLIENTE"));
			automovel.setPlaca(resultado.getString("PLACA"));
			automovel.setModelo(resultado.getString("MODELO"));
			automovel.setTipo(resultado.getString("TIPO"));
			automovel.setCapacidade(resultado.getInt("CAPACIDADE"));
			automovel.setEixos(resultado.getInt("EIXOS"));
			automovel.setDataRegistroEntrada(resultado.getString("DataDeRegistroEntrada"));
			automovel.setEntradaHorario(resultado.getString("HorarioEntrada"));
			automovel.setEntradaHora(resultado.getString("EntradaHora"));
			automovel.setMes(resultado.getString("MES"));
			automovel.setAnoEntrada(resultado.getString("ANO"));
			
		}
		if(automovel.getId()==0){
			automovel=null;
		}
		

		
		stm.close();
		conexao.close();
		
	}catch(SQLException ex){
		
		Logger.getLogger(VeiculoJDBCDAO.class.getName()).log(
				Level.SEVERE,null,ex);
	}
		return automovel;
	}

	@Override
	public boolean convert(String valor) {
		boolean x = true;
		try{
			int numero = Integer.parseInt(valor);
		}catch(NumberFormatException n){
			x = false;
		}
		
		return x;
	}

	@Override
	public void saveVeiculoHistorico(Veiculo entidade) {
		Connection conexao = new FabricaDeConexoes().getConnection("veiculos");

		

		try {

			String sql = "INSERT INTO historico(MODELO,PLACA,TIPO,CAPACIDADE,EIXOS,DataDeRegistroEntrada,HorarioEntrada,EntradaHora,MES,VALOR_PAGO,DataDeRegistroSaida,SaidaHorario,DuracaoHora,ANO) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement pstm = conexao.prepareStatement(sql);
			pstm.setString(1, entidade.getModelo());
			pstm.setString(2, entidade.getPlaca());
			pstm.setString(3, entidade.getTipo());
			pstm.setInt(4, entidade.getCapacidade());
			pstm.setInt(5, entidade.getEixos());
			pstm.setString(6, entidade.getDataRegistroEntrada());
			pstm.setString(7, entidade.getEntradaHorario());
			pstm.setString(8, entidade.getEntradaHora());
			pstm.setString(9, entidade.getMes());
			pstm.setString(10, entidade.getValorPago());
			pstm.setString(11, entidade.getDataRegistroSaida());
			pstm.setString(12, entidade.getSaidaHorario());
			pstm.setString(13, entidade.getDuracaoHora());
			pstm.setString(14, entidade.getAnoEntrada());

			pstm.execute();
			pstm.close();

			conexao.close();

		} catch (SQLException ex) {
			Logger.getLogger(VeiculoJDBCDAO.class.getName()).log(
					Level.SEVERE, null, ex);
			throw new RuntimeException();
		}
	
		
	}

	@Override
	public void deleteVeiculoID(int id) {
		Connection conexao = new FabricaDeConexoes().getConnection("veiculos");
		try {

			String sql = "DELETE FROM movimento WHERE ID_CLIENTE = "
					+ id;

			PreparedStatement pstm = conexao.prepareStatement(sql);

			pstm.execute();
			pstm.close();

		} catch (SQLException ex) {
			Logger.getLogger(VeiculoJDBCDAO.class.getName()).log(
					Level.SEVERE, null, ex);
		}
	}

	@Override
	public int quantEntradas() {
		int quantidade=0;
		try{
		Connection conexao = new FabricaDeConexoes().getConnection("veiculos");
		String sql = "SELECT COUNT(*) FROM movimento";
		PreparedStatement pstm = conexao.prepareStatement(sql);
		
	    ResultSet rc = pstm.executeQuery();
	    rc.next();
	      quantidade=rc.getInt(1);
	    
		}catch (SQLException ex) {
			Logger.getLogger(VeiculoJDBCDAO.class.getName()).log(
					Level.SEVERE, null, ex);
		}
	    

		return quantidade;
	}

	

}
