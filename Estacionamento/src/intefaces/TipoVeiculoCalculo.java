package intefaces;

import intefaces.dao.EstacionamentoJPADAO;
import entidade.Estacionamento;

public enum TipoVeiculoCalculo implements Calculo {
	
	
	PASSEIO{
EstacionamentoJPADAO localizar = new EstacionamentoJPADAO();
Estacionamento tabelaPreco = new Estacionamento();

	@Override
	public String veiculo(int hora, int minuto,int eixos, int capacidade) {
		tabelaPreco = localizar.find();
		double vlHora = Double.parseDouble(tabelaPreco.getVlPasseioHora());
		
		double vlTotalHora,vlTotalMinuto;
		String saida;
		vlTotalHora=hora*vlHora;
		vlTotalMinuto=0;
		if(minuto>5 && minuto<=30){
			vlTotalMinuto=vlHora/2;
		}else if(minuto>30){
			vlTotalMinuto=vlHora;
		}
		
		
		saida = String.format("%.2f",(vlTotalHora+vlTotalMinuto) );
		return saida;
	}
	},
	TRANSPORTE{
		EstacionamentoJPADAO localizar = new EstacionamentoJPADAO();
		Estacionamento tabelaPreco = new Estacionamento();
		@Override
		public String veiculo(int hora, int minuto,int eixos, int capacidade) {
			tabelaPreco = localizar.find();
			double vlHora = Double.parseDouble(tabelaPreco.getVlTransporteHora());
			double vlUnidadeCapc = Double.parseDouble(tabelaPreco.getVlUnidadePassageiro());
			double vlTotalHora,vlTotalMinuto,vlTotalCapacidade;
			String saida;
			vlTotalCapacidade = vlUnidadeCapc*capacidade;
			vlTotalHora=hora*vlHora;
			vlTotalMinuto=0;
			if(minuto>5 && minuto<=30){
				vlTotalMinuto=vlHora/2;
			}else if(minuto>30){
				vlTotalMinuto=vlHora;
			}
			saida = String.format("%.2f",(vlTotalHora+vlTotalMinuto+vlTotalCapacidade) );
			return saida;
		}
	},
	CAMINHAO{
		EstacionamentoJPADAO localizar = new EstacionamentoJPADAO();
		Estacionamento tabelaPreco = new Estacionamento();
		@Override
		public String veiculo(int hora, int minuto,int eixos, int capacidade) {
			tabelaPreco = localizar.find();
			double vlHora = Double.parseDouble(tabelaPreco.getVlCaminhaoHora());
			double vlUnidadeEixo = Double.parseDouble(tabelaPreco.getVlUnidadeEixo());
			double vlTotalHora,vlTotalMinuto,vlTotalEixos;
			String saida;
			vlTotalEixos = vlUnidadeEixo*eixos;
			vlTotalHora=hora*vlHora;
			vlTotalMinuto=0;
			if(minuto>5 && minuto<=30){
				vlTotalMinuto=vlHora/2;
			}else if(minuto>30){
				vlTotalMinuto=vlHora;
			}
			saida = String.format("%.2f",(vlTotalHora+vlTotalMinuto+vlTotalEixos) );
			return saida;
		}
	},
	MOTO{
		EstacionamentoJPADAO localizar = new EstacionamentoJPADAO();
		Estacionamento tabelaPreco = new Estacionamento();
		@Override
		public String veiculo(int hora, int minuto,int eixos, int capacidade) {
			tabelaPreco = localizar.find();
			double vlHora = Double.parseDouble(tabelaPreco.getVlMotoHora());
			double vlTotalHora,vlTotalMinuto;
			String saida;
			vlTotalHora=hora*vlHora;
			vlTotalMinuto=0;
			if(minuto>5 && minuto<=30){
				vlTotalMinuto=vlHora/2;
			}else if(minuto>30){
				vlTotalMinuto=vlHora;
			}
			saida = String.format("%.2f",(vlTotalHora+vlTotalMinuto) );
			return saida;
		}
	}

}
