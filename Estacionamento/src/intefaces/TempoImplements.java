package intefaces;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class TempoImplements implements Tempo {

	@Override
	public String duracao(String fim, String inicio) {
		long finall=Long.parseLong(fim);
		long inicial = Long.parseLong(inicio);
		String duracao;
			long horas = (finall - inicial) / 3600000;
		    long minutos = (finall - inicial - horas*3600000) / 60000;
		    duracao = String.valueOf(horas)+" hora(s) e"+String.valueOf(minutos+1)+" minuto(s)";
 	return duracao;
	}
	

	@Override
	public String entradaHorario() {
		
		Date agora = new Date();
		SimpleDateFormat modelo= new SimpleDateFormat("HH:mm");
		
		return modelo.format(agora);
	}

	@Override
	public String saidaHorario() {
		Date agora = new Date();
		SimpleDateFormat modelo= new SimpleDateFormat("HH:mm");
		
		return modelo.format(agora);
	}

	@Override
	public String dataRegistro() {
		Date agora = new Date();
		SimpleDateFormat modelo= new SimpleDateFormat("dd/MM/yyyy");
		
		return modelo.format(agora);
	}

	@Override
	public String mesRegistro() {
		String nomeMes;
		String meses[] = {"JANEIRO", "FEVEREIRO", 
	              "MAR�O", "ABRIL", "MAIO", "JUNHO", 
	              "JULHO", "AGOSTO", "SETEMBRO", "OUTUBRO",
		      "NOVEMBRO", "DEZEMBRO"};
		   
		   Calendar agora = Calendar.getInstance(); 
		   
	             nomeMes= meses[agora.get(Calendar.MONTH)];
		return nomeMes;
	}


	@Override
	public String entradaHoraMillisegundos() {
	Date agora = new Date();
	
	return String.valueOf(agora.getTime());
	}


	@Override
	public String saidaHoraMillisegundos() {
		Date agora = new Date();
		
		return String.valueOf(agora.getTime());
	}


	@Override
	public long duracaoHora(String fim, String inicio) {
		long finall=Long.parseLong(fim);
		long inicial = Long.parseLong(inicio);
		
	     long horas = (finall - inicial) / 3600000;
		 
		 
		return horas;
	}


	@Override
	public long duracaoMinuto(String fim, String inicio) {
		long finall=Long.parseLong(fim);
		long inicial = Long.parseLong(inicio);
				 
			long horas = (finall - inicial) / 3600000;
		    long minutos = (finall - inicial - horas*3600000) / 60000;
		   
		return minutos;
	}


	
	@Override
	public String getAno() {
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR); 
		String ano = String.valueOf(year);
		
		
		return ano;
	}

}
