package intefaces;

public interface Tempo {
	public String entradaHorario();	
	public String getAno();
	public String saidaHorario();
	public String dataRegistro();
	public String mesRegistro();
	public String entradaHoraMillisegundos();
	public String saidaHoraMillisegundos();
	public long duracaoHora(String fim, String inicio);
	public long duracaoMinuto(String fim, String inicio);

	public String duracao(String fim, String inicio);
	

}
