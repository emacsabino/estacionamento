package br.ifpi.entity;

public class Participante {
	public int id;
	public String cpf,nome, fone, perfil;
	public Participante(){
		
	}
	public Participante(String cpf, String nome,String fone, String perfil){
		this.cpf = cpf;
		this.nome = nome;
		this.fone = fone;
		this.perfil = perfil;
	}
	public void setID(int id){
		this.id = id;
	}
	public int getId(){
		return id;
	}
	public void setCPF(String cpf){
		this.cpf = cpf;
	}
	public String getCPF(){
		return cpf;
	}
	public void setFone(String fone){
		this.fone=fone;
	}
	public String getFone(){
		return fone;
	}
	public void setNOME(String nome){
		this.nome = nome;
	}
	public String getNome(){
		return nome;
	}
	public void setPERFIL(String perfil){
		this.perfil = perfil;
	}
	public String getPerfil(){
		return perfil;
	}
	public String toString(){
		return "ID:"+id+"\n"+"CPF:"+ cpf +"\n"+"NOME:"+ nome + "\n"+"FONE:"+ fone +"\n"+"PERFIL:"+ perfil+"\n"+"\n" ;
	}
	

}
