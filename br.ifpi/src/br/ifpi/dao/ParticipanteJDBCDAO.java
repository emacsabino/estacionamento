package br.ifpi.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.ifpi.entity.Participante;

public class ParticipanteJDBCDAO implements ParticipanteDAO {

	@Override
	public void save(Participante entity) {
		FabricaDeConexoes conector = new FabricaDeConexoes();
		String sql = "insert into participante" + "(cpf,nome,fone,perfil)"+"values(?,?,?,?)";
		try {
			PreparedStatement stmt = conector.conexao().prepareStatement(sql);
			stmt.setString(1,entity.getCPF());
			stmt.setString(2, entity.getNome());
			stmt.setString(3, entity.getFone());
			stmt.setString(4, entity.getPerfil());
			stmt.execute();
			stmt.close();
			
		} catch (SQLException e) {
	
			e.printStackTrace();
		}finally{
			try {
				conector.conexao().close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block//////////
				e.printStackTrace();
			}
		}
		
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(int id) {
		FabricaDeConexoes conector = new FabricaDeConexoes();
		String sql = "delete from participante where id="+id;
		try{
			PreparedStatement stm = conector.conexao().prepareStatement(sql);
			stm.execute();
			stm.close();
			
		}catch(SQLException e){
			System.out.println(e);
		}finally{
			try {
				conector.conexao().close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// TODO Auto-generated method stub
		
	}

	@Override
	public Participante find(int id) {
		FabricaDeConexoes conector = new FabricaDeConexoes();
		String sql = "select * from participante where id="+id;
		Participante pessoa = null;
		try {
			PreparedStatement stm = conector.conexao().prepareStatement(sql);
			ResultSet retorno = stm.executeQuery();
			while(retorno.next()){
				String cpf,nome,fone,perfil;
				
				cpf = retorno.getString("cpf");
				nome= retorno.getString("nome");
				fone=retorno.getString("fone");
				perfil=retorno.getString("perfil");
				
				 pessoa = new Participante(cpf,nome,fone,perfil);
				pessoa.setID(retorno.getInt("id"));
				
			}
			retorno.close();
			stm.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				conector.conexao().close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		// TODO Auto-generated method stub
		return pessoa;
	}

	@Override
	public List find() {
		FabricaDeConexoes conector = new FabricaDeConexoes();
		String sql = "select * from participante";
		List <Participante> participantes = new ArrayList<>();
		try {
			PreparedStatement stm = conector.conexao().prepareStatement(sql);
			ResultSet retorno = stm.executeQuery();
			while(retorno.next()){
				String cpf,nome,fone,perfil;
				
				cpf = retorno.getString("cpf");
				nome= retorno.getString("nome");
				fone=retorno.getString("fone");
				perfil=retorno.getString("perfil");
				
				Participante pessoa = new Participante(cpf,nome,fone,perfil);
				pessoa.setID(retorno.getInt("id"));
				participantes.add(pessoa);
			}
			retorno.close();
			stm.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				conector.conexao().close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return participantes;
	}

	@Override
	public Participante findByCpf(String cpf) {
		FabricaDeConexoes conector = new FabricaDeConexoes();
		String sql = "select * from participante where cpf="+cpf;
		Participante pessoa = null;
		try {
			PreparedStatement stm = conector.conexao().prepareStatement(sql);
			ResultSet retorno = stm.executeQuery();
			while(retorno.next()){
				String cpf1,nome,fone,perfil;
				
				cpf1 = retorno.getString("cpf");
				nome= retorno.getString("nome");
				fone=retorno.getString("fone");
				perfil=retorno.getString("perfil");
				
				 pessoa = new Participante(cpf1,nome,fone,perfil);
				pessoa.setID(retorno.getInt("id"));
				
			}
			retorno.close();
			stm.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				conector.conexao().close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return pessoa;
		
	}

	@Override
	public List findByNome(String nome) {
		FabricaDeConexoes conector = new FabricaDeConexoes();
		String sql = "SELECT * FROM participante WHERE NOME LIKE ?";
		List <Participante> participantes = new ArrayList<>();
		try {
			PreparedStatement stm = conector.conexao().prepareStatement(sql);
			stm.setString(1, "%"+nome + "%");
			ResultSet retorno = stm.executeQuery();
			while(retorno.next()){
				String cpf,nome1,fone,perfil;
				
				cpf = retorno.getString("cpf");
				nome1= retorno.getString("nome");
				fone=retorno.getString("fone");
				perfil=retorno.getString("perfil");
				
				Participante pessoa = new Participante(cpf,nome1,fone,perfil);
				pessoa.setID(retorno.getInt("id"));
				participantes.add(pessoa);
			}
			retorno.close();
			stm.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				conector.conexao().close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return participantes;
	}
		

}
