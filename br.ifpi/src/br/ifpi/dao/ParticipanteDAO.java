package br.ifpi.dao;

import br.ifpi.entity.Participante;


import java.util.List;

public interface ParticipanteDAO {
	
public void save (Participante entity); 
public void delete(int id);
public Participante find(int id);
public List find();
public Participante findByCpf(String cpf);
public List findByNome(String str);

	

}
