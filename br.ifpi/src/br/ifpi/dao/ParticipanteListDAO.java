package br.ifpi.dao;
import br.ifpi.entity.Participante;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

public class ParticipanteListDAO implements ParticipanteDAO  {
public static List <Participante> participantes;
int id=0;
public String mensagem;
public int mud,idExistente;
public ParticipanteListDAO(){
	participantes = new ArrayList<>();
}
public void save (Participante entity){
	if (mud==1){//indica que vai ser uma atuliza��o
		entity.setID(idExistente);//pega o id j� existente
		participantes.add(entity);
		JOptionPane.showMessageDialog(null, "Participante Atualizado Com Sucesso!");
	}
	else{
		this.id++;
		entity.setID(id);
		participantes.add(entity);
		JOptionPane.showMessageDialog(null, "Participante Cadastrado Com Sucesso!");
		
		
	}
		
}
public void delete(int id){
	
	
	for(Participante m: participantes){
		if(id==m.getId()){
			participantes.remove(m);
			break;
		}
	}
}
public Participante find(int id){
Participante dado=null;


for(Participante i : participantes){
	if(id==i.getId()){
		dado = i;
		
	}
}
return dado;
}
public List find(){
	return this.participantes;
}
public Participante findByCpf(String cpf){
	Participante dado=null;
	
	
	
	for(Participante entity: participantes){
		
		if(entity.getCPF().equals(cpf)){
			
			dado=entity;
			
			break;
		}
		
	}
	return dado;
}
public List findByNome(String str){
	List<Participante> nomesParte = new ArrayList();
	for(Participante i: participantes){
		if(i.getNome().contains(str)){
			nomesParte.add( i );
		}
	}
	return nomesParte;
}

}
