package br.ifpi.formularios;
import javax.swing.*;

import br.ifpi.entity.Participante;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class FormAtualizar extends form {
public static	JFrame area = new JFrame("Cadastro");
public static JTextField nom,cpf,perfil,fone,cpfdig;
public static JLabel nomL,cpfL,perfilL,foneL,cadastro,cpfDig;
public static String cpf3,nome,fone2, perfil2;


	JPanel painel2=new JPanel();
	public static Participante pessoa;
	public void atualizar(){
		
		JButton cad = new JButton("INSERIR");
		JButton busc = new JButton("Buscar");
		
		area.setContentPane(painel2);
		painel2.setLayout(new GridBagLayout());
		GridBagConstraints confi = new GridBagConstraints();
		cpfDig = new JLabel("Digite o CPF");
		cpfdig = new JTextField(15);
		confi.gridx=1;
		confi.gridy=10;
		confi.insets = new Insets(20,20,20,20);
		cadastro = new JLabel("ATUALIZAÇÃO");
		cadastro.setForeground(Color.blue);
		painel2.add(cadastro,confi);
		
		confi.insets = new Insets(5,5,5,5); 
		confi.gridx=0;
		confi.gridy=15;
		confi.anchor = GridBagConstraints.NORTHWEST; 
		painel2.add(cpfDig,confi);
		
		confi.gridx=0;
		confi.gridy=20;
		cpfL = new JLabel("CPF");
		painel2.add(cpfL,confi);
		
		
		confi.gridy=23;
		nomL=new JLabel("Nome");
		painel2.add(nomL,confi);
		
		confi.gridy=26;
		foneL = new JLabel("Fone");
		painel2.add(foneL,confi);
		confi.gridy=29;
		
		
		perfilL = new JLabel("Perfil");
		painel2.add(perfilL,confi);
		confi.gridx=1;
		confi.gridy=15;
		painel2.add(cpfdig,confi);
		
		confi.gridx=1;
		confi.gridy=20;
		
		cpf = new JTextField(15);
		painel2.add(cpf,confi);
		confi.gridy=23;
		nom = new JTextField(20);
	    painel2.add(nom,confi);
	    
	    
		confi.gridy=26;
		fone = new JTextField(20);
		painel2.add(fone,confi);
		confi.gridy=29;
		perfil = new JTextField(20);
		painel2.add(perfil,confi);
		confi.insets = new Insets(20,20,20,20); 
		confi.gridy=60;
		confi.gridx=0;
		
		
		 cad.addActionListener(new ActionListener(){
				
			 public void actionPerformed(ActionEvent e){
				
				cpf3=cpf.getText();
				nome = nom.getText();
				fone2=fone.getText();
				perfil2=perfil.getText();
				pessoa = new Participante(cpf3,nome,fone2,perfil2);
				cadas.save(pessoa);
				cpf.setText(null);
				nom.setText(null);
			    fone.setText(null);
				perfil.setText(null);
				cpfdig.setText(null);
				cadas.mud=0;
				
				 
				 
			 }
		 });
		 busc.addActionListener(new ActionListener(){
				
			 public void actionPerformed(ActionEvent e){
				 Participante p1;
				
					
					p1 = cadas.findByCpf(cpfdig.getText());
					if(p1 != null){
						cpf.setText(p1.getCPF());
						nom.setText(p1.getNome());
						fone.setText(p1.getFone());
						perfil.setText(p1.getPerfil());
						cadas.mud=1;
						cadas.idExistente=p1.getId();
						cadas.delete(p1.getId());
						
					} else {
						JOptionPane.showMessageDialog(null,"Nao ha um participante com este CPF!");
						
						
					}
				 
			 }
		 });
		 confi.anchor = GridBagConstraints.CENTER;
		painel2.add(cad,confi);
		confi.gridy=60;
		confi.gridx=1;
		
		painel2.add(busc,confi);
		
		
		
	
		
		area.setSize(500, 400);
		area.setVisible(true);
		
	}
	

}
