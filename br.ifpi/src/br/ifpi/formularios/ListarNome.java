package br.ifpi.formularios;

import javax.swing.*;

import br.ifpi.dao.ParticipanteJDBCDAO;
import br.ifpi.entity.Participante;

import java.awt.*;
import java.awt.event.*;
import java.util.List;


public class ListarNome extends form {
	
	public static JLabel rotulo;
	
	public static JTextArea saida5 = new JTextArea(20,40);
	public static JTextField campo = new JTextField();
	
public  void mostrar(){
	
	JFrame tela3 = new JFrame("Listar");
	
	
	JButton mostrar = new JButton("Buscar");


Container tela = tela3.getContentPane();
tela3.setLayout(null);
rotulo = new JLabel("Busca por nome");


mostrar = new JButton("Mostrar");

rotulo.setBounds(130,20,100,20);

campo.setBounds(160,50,150,20);
mostrar.setBounds(50,50,80,20);
mostrar.addActionListener(
new ActionListener(){
public void actionPerformed(ActionEvent e){
	
	 List<Participante> part;
		String nome = campo.getText();
		ParticipanteJDBCDAO nomeTudo = new ParticipanteJDBCDAO();
		part = nomeTudo.findByNome(nome);
		if(part.isEmpty()){
			saida5.setText("Nome n�o encontrado");
		}
		else{
			
			saida5.setText("Participantes"+"\n"+"\n"+part.toString());
		}
	

}
}
);
JPanel painel3 = new JPanel();

JScrollPane rolagem = new JScrollPane(saida5);
rolagem.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
rolagem.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
painel3.setBounds(5,130,600,800);
painel3.add(rolagem);

tela.add(rotulo);
tela.add(painel3);
tela.add(campo);

tela.add(mostrar);
tela3.setSize(600, 600);
tela3.setVisible(true);
tela3.setLocationRelativeTo(null);
}
}