package br.ifpi.formularios;
import javax.swing.*;

import br.ifpi.dao.ParticipanteJDBCDAO;
import br.ifpi.dao.ParticipanteListDAO;
import br.ifpi.entity.Participante;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class form {
public static	JFrame area = new JFrame("Cadastro");
public static JTextField nom,cpf,perfil,fone;
public static JLabel nomL,cpfL,perfilL,foneL,cadastro;
public static String cpf3,nome,fone2, perfil2;

public static ParticipanteListDAO cadas = new ParticipanteListDAO();
	JPanel painel2=new JPanel();
	public static Participante pessoa;
	public ImageIcon imagem = new ImageIcon(getClass().getResource("/imagem/informatica_imagem.jpg"));
	public void cadastro(){
		
		JButton cad = new JButton("INSERIR");
		
		area.setContentPane(painel2);
		painel2.setLayout(new GridBagLayout());
		GridBagConstraints confi = new GridBagConstraints();
		
		confi.gridx=10;
		confi.gridy=0;
		confi.insets = new Insets(20,20,20,20);
		cadastro = new JLabel("CADASTRO");
		cadastro.setForeground(Color.blue);
		painel2.add(cadastro,confi);
		confi.gridx=0;
		confi.gridy=15;
		confi.insets = new Insets(5,5,5,5); 
		
		confi.anchor = GridBagConstraints.NORTHWEST; 
		cpfL = new JLabel("CPF");
		painel2.add(cpfL,confi);
		
		
		confi.gridy=18;
		nomL=new JLabel("Nome");
		painel2.add(nomL,confi);
		
		confi.gridy=21;
		foneL = new JLabel("Fone");
		painel2.add(foneL,confi);
		confi.gridy=24;
		perfilL = new JLabel("Perfil");
		painel2.add(perfilL,confi);
		confi.gridx=10;
		confi.gridy=15;
		cpf = new JTextField(15);
		painel2.add(cpf,confi);
		confi.gridy=18;
		nom = new JTextField(30);
	    painel2.add(nom,confi);
	    
	    
		confi.gridy=21;
		fone = new JTextField(30);
		painel2.add(fone,confi);
		confi.gridy=24;
		perfil = new JTextField(30);
		painel2.add(perfil,confi);
		confi.insets = new Insets(20,20,20,20); 
		confi.gridy=60;
		confi.gridx=10;
				confi.weightx = 1;  
		confi.weighty = 1;
		confi.anchor = GridBagConstraints.CENTER; 
		 cad.addActionListener(new ActionListener(){
				
			 public void actionPerformed(ActionEvent e){
				ParticipanteJDBCDAO inserirDados = new ParticipanteJDBCDAO();
				cpf3=cpf.getText();
				nome = nom.getText();
				fone2=fone.getText();
				perfil2=perfil.getText();
				pessoa = new Participante(cpf3,nome,fone2,perfil2);
				inserirDados.save(pessoa);
				cpf.setText(null);
				nom.setText(null);
			    fone.setText(null);
				perfil.setText(null);
				
				
				 
				 
			 }
		 });
		painel2.add(cad,confi);
		
		
		
		
		
	
		
		area.setSize(500, 400);
		area.setVisible(true);
		
	}
	

}
