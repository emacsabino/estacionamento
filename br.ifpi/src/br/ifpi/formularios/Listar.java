package br.ifpi.formularios;

import javax.swing.*;

import br.ifpi.dao.ParticipanteJDBCDAO;
import br.ifpi.entity.Participante;

import java.awt.*;
import java.awt.event.*;


public class Listar extends form {
	
	public static JLabel rotulo;
	public static String cpf5;
	public static JTextArea saida5 = new JTextArea(20,40);
	public static JTextField campo = new JTextField();
	
public  void mostrar(){
	
	JFrame tela3 = new JFrame("Listar");
	
	
	JButton mostrar = new JButton("Buscar");


Container tela = tela3.getContentPane();
tela3.setLayout(null);
rotulo = new JLabel("Busca pelo CPF");


mostrar = new JButton("Mostrar");

rotulo.setBounds(130,20,100,20);

campo.setBounds(160,50,150,20);
mostrar.setBounds(50,50,80,20);
mostrar.addActionListener(
new ActionListener(){
public void actionPerformed(ActionEvent e){
	Participante p1;
	 cpf5=campo.getText();
	 ParticipanteJDBCDAO cpfMostrar = new ParticipanteJDBCDAO();
		
		p1 = cpfMostrar.findByCpf(cpf5);
		if(p1 != null){
			saida5.setText( "Dados do Participante!"
					+ "\n\nInforma��es"
					+ "\nId: " + p1.getId()
					+ "\nNome: " + p1.getNome()
					+ "\nCPF: " + p1.getCPF()
					+ "\nTelefone: " + p1.getFone()
					+ "\nPerfil: " + p1.getPerfil());
		} else {
			saida5.setText("Esse participante n�o existe");
			
			
		}

}
}
);
JPanel painel3 = new JPanel();

JScrollPane rolagem = new JScrollPane(saida5);
rolagem.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
rolagem.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
painel3.setBounds(5,130,600,800);
painel3.add(rolagem);

tela.add(rotulo);
tela.add(painel3);
tela.add(campo);

tela.add(mostrar);
tela3.setSize(600, 600);
tela3.setVisible(true);
tela3.setLocationRelativeTo(null);
}
}