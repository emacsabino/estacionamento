package br.ifpi.formularios;

import javax.swing.*;

import br.ifpi.dao.ParticipanteJDBCDAO;
import br.ifpi.entity.Participante;

import java.awt.*;
import java.awt.event.*;
import java.util.List;


public class ListarTudo extends form {
	
	public static JLabel rotulo;
	
	public static JTextArea saida5 = new JTextArea(20,40);
	
/**
 * @wbp.parser.entryPoint
 */
public  void mostrar(){
	
	JFrame tela3 = new JFrame("Listar");
	
	
	JButton mostrar = new JButton("Buscar");


Container tela = tela3.getContentPane();
tela3.getContentPane().setLayout(null);
rotulo = new JLabel("Listar Todos");
rotulo.setFont(new Font("Tarzan", Font.PLAIN, 27));


mostrar = new JButton("Mostrar");

rotulo.setBounds(218,44,183,20);
mostrar.setBounds(266,75,80,20);
mostrar.addActionListener(
new ActionListener(){
public void actionPerformed(ActionEvent e){
	ParticipanteJDBCDAO mostrarTudo = new ParticipanteJDBCDAO();
	 List<Participante> part;
		
		part = mostrarTudo.find();
		if(part.isEmpty()){
			saida5.setText("Nome n�o encontrado");
		}
		else{
			
			saida5.setText("Participantes"+"\n"+"\n"+part.toString());
		}
	

}
}
);
JPanel painel3 = new JPanel();

JScrollPane rolagem = new JScrollPane(saida5);
rolagem.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
rolagem.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
painel3.setBounds(5,130,600,800);
painel3.add(rolagem);

tela.add(rotulo);
tela.add(painel3);

tela.add(mostrar);
tela3.setSize(600, 600);
tela3.setVisible(true);
tela3.setLocationRelativeTo(null);
}
}