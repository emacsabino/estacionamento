package br.ifpi.formularios;

import javax.swing.*;

import br.ifpi.dao.ParticipanteJDBCDAO;
import br.ifpi.entity.Participante;

import java.awt.*;
import java.awt.event.*;



public class ListarPorID extends form {
	
	public static JLabel rotulo;
	
	public static JTextArea saida5 = new JTextArea(20,40);
	public static JTextField campo = new JTextField();
	
public  void mostrar(){
	
	JFrame tela3 = new JFrame("Listar");
	
	
	JButton mostrar = new JButton("Buscar");


Container tela = tela3.getContentPane();
tela3.setLayout(null);
rotulo = new JLabel("Busca por ID");


mostrar = new JButton("Mostrar");

rotulo.setBounds(130,20,100,20);

campo.setBounds(160,50,150,20);
mostrar.setBounds(50,50,80,20);
mostrar.addActionListener(
new ActionListener(){
public void actionPerformed(ActionEvent e){
	
	 int id;
		Participante p;
		ParticipanteJDBCDAO idMostrar = new ParticipanteJDBCDAO();
		id = Integer.parseInt(campo.getText());
		p = idMostrar.find(id);
		if(p != null){
			saida5.setText("Dados Pessoais"
					+ "\n\nInformações"
					+ "\nId: " + p.getId()
					+ "\nNome: " + p.getNome()
					+ "\nCPF: " + p.getCPF()
					+ "\nTelefone: " + p.getFone()
					+ "\nPerfil: " + p.getPerfil());
		} else {
			saida5.setText("Nao ha um participante com este ID!");
		}
	

	
	}
}
);
JPanel painel3 = new JPanel();

JScrollPane rolagem = new JScrollPane(saida5);
rolagem.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
rolagem.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
painel3.setBounds(5,130,600,800);
painel3.add(rolagem);

tela.add(rotulo);
tela.add(painel3);
tela.add(campo);

tela.add(mostrar);
tela3.setSize(600, 600);
tela3.setVisible(true);
tela3.setLocationRelativeTo(null);
}
}