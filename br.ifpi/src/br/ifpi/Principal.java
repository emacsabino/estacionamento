package br.ifpi;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;





import javax.swing.*;

import br.ifpi.dao.ParticipanteListDAO;
import br.ifpi.formularios.FormAtualizar;
import br.ifpi.formularios.Listar;
import br.ifpi.formularios.ListarNome;
import br.ifpi.formularios.ListarPorID;
import br.ifpi.formularios.ListarTudo;
import br.ifpi.formularios.RemoverCPF;
import br.ifpi.formularios.form;


public class Principal {
	
	
	
	public static ParticipanteListDAO ver = new ParticipanteListDAO();
	
 
	
	public static void main(String [] args){
		
		JLabel icone;
		form foto = new form();
		icone = new JLabel(foto.imagem);
		
		
		JFrame tela = new JFrame("Menu");//cria o objeto da classe JFrame
		tela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel painel = new JPanel(new BorderLayout());//Componente para agrupar na janela outros componentes
        JMenuBar barra = new JMenuBar();//cria o objeto para inserir a barra de menus
		
		JMenu localizar = new JMenu("Localizar");
		JMenu novo = new JMenu("Novo/Editar");
		JMenu outros = new JMenu("Outras Op��es");
	
		tela.setContentPane(painel);//insere no objeto tela
		painel.add(icone,BorderLayout.CENTER);
		tela.setSize(600,400);//dimensiona o tamanho da tela
		
		tela.setJMenuBar(barra);//insere a barra na tela principal
		
		 JMenuItem inserirParticipante = new JMenuItem("Inserir");
		inserirParticipante.setForeground(Color.blue);//muda a cor do fundo para preto
		
		
		
		JMenuItem atualizar = new JMenuItem("Atualizar Dados por CPF");//cria o objeto menu que vai ser colocado na barra
		atualizar.setForeground(Color.blue);//muda a cor do texto no menu
		
		JMenuItem removerCPF = new JMenuItem("Remover por CPF");
		removerCPF.setForeground(Color.blue);
		JMenuItem localizarCPF =new JMenuItem("Localizar por CPF");
		localizarCPF.setForeground(Color.blue);
		JMenuItem localizarID = new JMenuItem("Localizar  por ID");
		localizarID.setForeground(Color.blue);
		JMenuItem localizarSTRING = new JMenuItem("Localizar por Nome");
		localizarSTRING.setForeground(Color.blue);
		JMenuItem sair = new JMenuItem("Sair");
		sair.setForeground(Color.blue);
		
		
		barra.add(novo);//insere os objetos criados na barra
		barra.add(localizar);
		barra.add(outros);
		localizar.add(localizarSTRING);//insere op��es dentro das primeiras op��es
		localizar.add(localizarCPF);
		localizar.add(localizarID);
		
		JMenuItem mntmLocalizarTodos = new JMenuItem("Localizar Todos");
		mntmLocalizarTodos.setForeground(Color.BLUE);
		localizar.add(mntmLocalizarTodos);
		
		novo.add(inserirParticipante);
		novo.add(atualizar);
		outros.add(removerCPF);
	
		
		outros.add(sair);
		
		 inserirParticipante.addActionListener(new ActionListener(){
			
			 public void actionPerformed(ActionEvent e){
				form pag = new form();
				pag.cadastro();
				 
						
					
					
			 }
		 });
		 atualizar.addActionListener(new ActionListener(){
			 
			 public void actionPerformed(ActionEvent e){
				
				FormAtualizar form2 = new FormAtualizar();
				form2.atualizar();
			 }
		 });
		 localizarCPF.addActionListener(new ActionListener(){
				
			 public void actionPerformed(ActionEvent e){
				Listar exibir = new Listar();
				exibir.mostrar();
				 
				
			 }
		 });
		 mntmLocalizarTodos.addActionListener(new ActionListener(){
				
			 public void actionPerformed(ActionEvent e){
				 ListarTudo exibirTodos = new ListarTudo();
				exibirTodos.mostrar();
				 
				
			 }
		 });
		 sair.addActionListener(new ActionListener(){
				
			 public void actionPerformed(ActionEvent e){
				 
				 System.exit(0);
					
			 }
		 });
		 localizarID.addActionListener(new ActionListener(){
			 
			 public void actionPerformed(ActionEvent e){
				
				ListarPorID form6=new ListarPorID();
					form6.mostrar();
			 }
		 });
		
		 removerCPF.addActionListener(new ActionListener(){
			 
			 public void actionPerformed(ActionEvent e){
				 
			RemoverCPF form8 = new RemoverCPF();
			form8.remover();
					
			 }
		 });
		 localizarSTRING.addActionListener(new ActionListener(){
				
			 public void actionPerformed(ActionEvent e){
				
				ListarNome form4 = new ListarNome();
				form4.mostrar();
					
			 }
		 });
		 tela.setVisible(true);//deixa a tela vis�vel
		 
	}
}